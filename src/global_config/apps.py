from django.apps import AppConfig

class GlobalConfigurationConfig(AppConfig):
    name = 'global_config'
    label = "global_config"
    verbose_name = "Global Config"