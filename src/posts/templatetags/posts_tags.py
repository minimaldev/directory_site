
from django.conf import settings
from django import template
from django.utils import timezone
from django.db.models import Q
#from posts.models import PostCategory
from posts.models import PostItem
from utilities.paginator import paginator
from utilities.media import get_oembed
import mistune

register = template.Library()

@register.assignment_tag(name='get_categories_nav')
def get_categories_nav():
    f1 = Q(publish=True)
    return PostCategory.objects.filter(f1)

@register.assignment_tag(name='get_features_articles', takes_context=True)
def get_features_articles(context, limit=5):
    current_date =  timezone.now()
    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(post_type="post") 
    if context.has_key('post'):
        return PostItem.objects.filter(  f1 & (f2 | f3) & f4 ).exclude(id=context['post'].id).order_by("-publish_date",'-id')[:limit]
    else:
        return PostItem.objects.filter(  f1 & (f2 | f3) & f4 ).order_by("-publish_date",'-id')[:limit]

@register.assignment_tag(name='get_recent_articles', takes_context=True)
def get_recent_articles(context, limit=10):
    page = context['page']
    #lt for less than
    current_date =  timezone.now()
    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)     
    f4 = Q(post_type="post")  
    return paginator(
            page, 
            PostItem.objects.filter(f1 & f4 & (f2 | f3) ).order_by("-publish_date",'-id'),
            limit
        )

@register.assignment_tag(name='get_oembed_object', takes_context=True)
def get_oembed_object(context, item):
    request = context['request']
    BotNames=[
        'Googlebot',
        'Slurp',
        'Twiceler',
        'msnbot',
        'KaloogaBot',
        'YodaoBot',
        '"Baiduspider',
        'googlebot',
        'Speedy Spider',
        'DotBot'
    ]
    user_agent=request.META.get('HTTP_USER_AGENT',None)
    if not user_agent in BotNames:
        return  get_oembed(item)
    return  item

@register.filter
def get_markdown(item):
    renderer = mistune.Renderer(escape=False, hard_wrap=True)
    markdown = mistune.Markdown(renderer=renderer)
    return markdown(item)