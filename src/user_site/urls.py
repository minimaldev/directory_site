from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls import patterns
from user_site import views

urlpatterns = patterns('',

	url(r'^$',
        views.my_profile,
        #{'template_name': 'modules/user_site/profile.html'},
        name='user_site_auth_profile'
    ),

    url(r'^/add/profile/image$',
        views.add_item_profile_image,
        name='add_item_profile_image'
    ),    
    
    url(r'^login/ajax/$',
        views.login_ajax,     
        name='user_site_auth_login_ajax'
    ),
    
    url(r'^registration/ajax/$',
        views.registration_ajax,     
        name='user_site_auth_registration_ajax'
    ),

    url(r'^user/is_autenticated/$',
        views.user_is_autenticated,     
        name='user_site_auth_is_autenticated'
    )
)