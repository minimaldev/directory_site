from django.core.management.base import NoArgsCommand
from django.db import connection
from posts.models import PostItem
from django.db.models import Count
from django.conf import settings

from directory.models import DirectoryCategory
from directory.models import DirectoryItem
from media.models import MediaAlbum
from media.models import MediaImage
from user_site.models import UserSite
import random
import datetime

from django.utils import timezone

class Command(NoArgsCommand):
    help = 'check youtube videos'

    def handle(self, *args, **options):
        images = [
            '2-03GCR38H001OPTIMIZADOS.jpg',
            '3-06GCR38H001OPTIMIZADOS.jpg',
            '05GCR38H001OPTIMIZADOS.jpg',
            '08GCR38H001OPTIMIZADOS.jpg',
            '09GCR38H001OPTIMIZADOS.jpg',
            'aaf_1-PORTADA02GCR38H001OPTIMIZADOS.jpg',
            '258242FESTIVA4OPTIMIZADO.jpg',
            '258242FESTIVA2OPTIMIZADO.jpg',
            '258242FESTIVA3OPTIMIZADO.jpg',
        ]
        media_images = []
        media_albums = []
        for x in range(3):
            album = MediaAlbum()
            album.title = "Test"
            album.save()
            media_albums.append(album)
         
        for x in range(100):
            image = MediaImage()
            image.image = 'uploads/%s' % ( random.choice(images))
            image.title = "Sample image"
            #image.album.add(random.choice(media_albums))
            image.save()
            media_images.append(image)

        images = random.shuffle(media_images)

        categories = DirectoryCategory.objects.filter(id__in=[1,2,3,4])

        autor = UserSite.objects.get(id=1)
        for x in range(10000000):
            t = datetime.datetime.now().time()
            now = timezone.now()
            item = DirectoryItem()
            item.autor = autor
            item.title = "sample advert %s " % (t)
            item.price = 100
            item.featured_start_date = now
            item.featured_end_date = now

            item.phone = "55555555"
            item.slug = "sample-advert-%s" % (t)
            #item.album.add(random.choice(media_albums))
            #for C in categories:
            #    item.survey.add(C)            
            item.save()
        pass