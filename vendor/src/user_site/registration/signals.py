from django.dispatch import Signal


# A new user has registered.
user_site_registered = Signal(providing_args=["user_site", "request"])

# A user has activated his or her account.
user_site_activated = Signal(providing_args=["user_site", "request"])
