
from celery import task
from celery.decorators import periodic_task
from django.conf import settings
from src.utilities.send_email import send_mail
from src.user_site.models import UserSite
from newsletter.models import EmailNewsletter
from directory.models import DirectoryItem
from django.utils import timezone
from datetime import  timedelta
from django.utils.translation import ugettext_lazy as _

@periodic_task(run_every=timedelta(days=3), name='newsletter')
def newsletter():
    
    limit = 3
    limit_new = 5
    current_date = timezone.now()
    template_name = "directory/newsletter"
    subject = _('YOUR_NEWSLETTER_LABEL')
    context = {}
    
    context['featured_items'] = DirectoryItem.objects.filter(                   
        publish=True,
        featured_end_date__lte=current_date
    ).order_by("-featured_end_date", "-publish_date",'-id')[:limit]
    
    ids = [int(x.id) for x in context['featured_items']]
    
    context['new_items'] = DirectoryItem.objects.filter(                   
        publish=True,
        release_date__lte=current_date
    ).exclude(id__in=ids).order_by("-publish_date",'-id')[:limit_new]
   
    for e in EmailNewsletter.objects.filter(suscribed=True):
        send_email(
           context=context,
           template=template_name, 
           subject=subject, 
           from_email=settings.DEFAULT_FROM_EMAIL,
           to_email=e.email
        )

   pass
