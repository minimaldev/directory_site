from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from user.models import CustomUser as User
from django.utils.translation import ugettext_lazy as _
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'nick', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email','logo', 'autor_info')}),
        (_('Permissions'), {'fields': (
        	'is_active', 'is_staff', 'is_superuser',
            'groups', 'user_permissions'
            )}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'nick', 'logo', 'autor_info', 'password1', 'password2'),
        }),
    )
    list_display = ('username', 'nick', 'email', 'first_name', 'last_name', 'is_staff')

admin.site.register(User, CustomUserAdmin)