from django.conf import settings
from user_site import authenticate
from user_site import login
from user_site.models import UserSite as User
from user_site.registration import signals
from user_site.registration.views import RegistrationView as BaseRegistrationView

class RegistrationView(BaseRegistrationView):
    def register(self, request, **cleaned_data):
        email, password =  cleaned_data['email'], cleaned_data['password1']
        User.objects.create_user (email, password)
        new_user = authenticate(email=email, password=password)
        login(request, new_user)
        signals.user_site_registered.send(
                sender=self.__class__,
                user_site=new_user,
                request=request
            )
        return new_user

    def registration_allowed(self, request):
        return getattr(settings, 'REGISTRATION_OPEN', True)

    def get_success_url(self, request, user):
        return (user.get_absolute_url(), (), {})
