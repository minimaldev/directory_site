from django import template
from blog.models import BlogCategory
from blog.models import BlogPage
register = template.Library()

#@register.inclusion_tag('blog/templatetags/breacrumb.html', takes_context = True)
#def render_breadcrumb_tag(context):	
#	slug=context['Slug']
#	breadcrumb = enumerate(BlogCategory.get_ancestors(ascending=False, include_self=True))
#	if context.has_key('Breadcrumb_last_item'):
#		last_item=context['Breadcrumb_last_item']
#		return {'breadcrumb': breadcrumb,'last_item':last_item}
#	return {'breadcrumb': breadcrumb}

@register.inclusion_tag('blog/templatetags/categories.html', takes_context = True)
def render_categories_tag(context):	
	return {'categories':  BlogCategory.objects.all()}


@register.inclusion_tag('blog/templatetags/pages.html', takes_context = True)
def render_pages_tag(context):	
	return {'pages':  BlogPage.objects.all()}