from django.core.management.base import NoArgsCommand
from rss_youtube_videos.models import VideoChannel
from rss_youtube_videos.models import VideoChannelVisits
from rss_youtube_videos.models import VideoItem
from unidecode import unidecode
from django.template.defaultfilters import slugify
from django.db.models import Count
from django.utils import timezone
from datetime import  timedelta
from django.conf import settings
import urllib
import httplib
import json
from urlparse import urlparse
"""
rurn command checkyoutubevideos
15 * * * * /usr/bin/env python /home/minimaldev/Desktop/www/PYTHON/cms-django-3/manage.py checkyoutubevideos

"""


class Command(NoArgsCommand):
    help = 'check youtube videos'

    def handle(self, *args, **options):
 
        current_date = timezone.now()
        current_hour = current_date.strftime('%H:%M:%S')
        current_day = current_date.strftime('%Y-%m-%d')
        videos_channels = VideoChannel.objects.all()
        for channel in  videos_channels:
            if self.is_the_same_day_but_dont_have_visits(channel, current_day) == True and self.is_the_same_day_but_dont_have_anyone_visits(channel, current_day) == False:
                self.save_videos(channel, current_date)
            if self.is_the_same_day_but_they_have_visits(channel, current_day) == True:
                self.save_videos(channel, current_date)

            if self.is_the_same_day_but_dont_have_anyone_visits(channel, current_day) == True :
                self.save_videos(channel, current_date)

        self.stdout.write('Successfully Command Executed!!!' )

    def save_videos(self,channel, current_date):
        channel_visit_hour_list = map(int, channel.visit_hour.split(':'))
        channel_hour = channel_visit_hour_list[0]
        channel_minute = channel_visit_hour_list[1]
        channel_second = channel_visit_hour_list[2]
        channel_visit_hour_comprare = current_date.replace(
                hour=channel_hour, 
                minute=channel_minute, 
                second=channel_second, 
                microsecond=0
            )
        #cantidad de horas intermedias para calcular la siguiente hora de visita
        between_hours = abs(channel_hour - current_date.hour)
        """ si la hora actual es igual o mayor a la hora de visita entoces si revisa
            y guarda el video
        """
        if current_date >= channel_visit_hour_comprare:
            playlist_items_url = 'https://www.googleapis.com/youtube/v3/playlistItems'

            playlist_params = {
                'part': 'snippet,contentDetails',
                'key' : settings.GOOGLE_API_KEY,
                'maxResults': 1,
                'playlistId' : channel.upload_playlist_id
                }

            playlist_items_data = self.url_sender (playlist_items_url, playlist_params)

            video_title = playlist_items_data['items'][0]['snippet']['title']
            channel_title = playlist_items_data['items'][0]['snippet']['channelTitle']
            video_url = playlist_items_data['items'][0]['contentDetails']['videoId']             
            thumbnail = playlist_items_data['items'][0]['snippet']['thumbnails']
            description = playlist_items_data['items'][0]['snippet']['description']
            video_url = 'https://www.youtube.com/watch?v=%s' % video_url
            item_count = len(VideoItem.objects.filter(link = video_url).annotate(created_count = Count('id')))

            if item_count == 0:
               video = VideoItem()
               video.channel = channel
               video.title = unidecode(video_title +' en ' + channel_title)
               video.metatitle = video.title
               video.slug =  slugify(video.title)
               video.link = video_url
               video.thumb = thumbnail
               video.excerpt = 'Hola %s ha publicado un video interestante te gustaria verlo?' % channel_title
               video.save()
               self.stdout.write('Successfully Youtube Video Added!!!' )

            channel.last_visit_date = current_date
            channel.next_visit_date = current_date + timedelta(hours=between_hours)
            
            if channel.visit_counter >= 0 and channel.visit_counter <= int(channel.visit_per_week):
                channel.visit_counter = channel.visit_counter + 1
            
            if channel.visit_counter >= 1 and channel.visit_counter >= int(channel.visit_per_week):
                channel.visit_counter = 0

            channel.save()
      
    def check_date(self, channel, date_to_check):
        if getattr(channel, 'next_visit_date', None) is not None:
            return date_to_check >= channel.next_visit_date
        else:
            return False

    def is_the_same_day(self, channel, day_to_check):
        if getattr(channel, 'next_visit_date', None) is not None:
            return day_to_check >= channel.next_visit_date.strftime('%Y-%m-%d')
        else:
            return False
    # es el mismo dia pero no tiene visitas 
    def is_the_same_day_but_dont_have_visits(self, channel, day_to_check):
        if getattr(channel, 'next_visit_date', None) is not None:
            return (
                self.is_the_same_day(channel, day_to_check) == True
                and (channel.visit_counter >= 0 and channel.visit_counter <= int(channel.visit_per_week)) == True
                )

        else:
            return False
    def is_the_same_day_but_dont_have_anyone_visits(self, channel, day_to_check):
        return (getattr(channel, 'next_visit_date', None) is None 
                and (channel.visit_counter == 0 and channel.visit_counter <= int(channel.visit_per_week)) == True)
    def is_the_same_day_but_they_have_visits(self, channel, day_to_check):
        if getattr(channel, 'next_visit_date', None) is not None:
            return (
                self.is_the_same_day(channel, day_to_check) == True
                and (channel.visit_counter >= 1 and channel.visit_counter <= int(channel.visit_per_week)) == True
            )
        else:
            return False
    # es el mismo dia pero no tiene visitas
    def url_sender (self, url, parameters):
        query = urllib.urlencode(parameters,True)
        URL = urlparse(url)
        connection = httplib.HTTPSConnection(URL.hostname)
        connection.request('GET', URL.path + '?' + query)
        response = connection.getresponse()
        return json.loads(response.read())
