import re
from django.conf import settings
from django import template
from django.utils import timezone
from bs4 import BeautifulSoup, Tag
from django.db.models import Q
from media.models import MediaImage
from utilities.media import get_oembed_html
from utilities.media import get_oembed
import mistune

register = template.Library()
regex = re.compile(r'\[album\](.*?)\[\/album\]+')
regex2 = re.compile(r'\<p\>(\[album\].*\[\/album\])\<\/p\>+')
"""
ussage |get_markdown|album_short_code
this need to be improved
"""
@register.filter
def album_short_code(content, replace_parent=False):
   
    for item in regex2.findall(unicode(content)):
        g = re.search(regex,item) 
        pk = g.group(1)
        image = ""
        images = MediaImage.objects.filter(album__id=pk).order_by('id')
        
        if len(images)>0:
            
            for img in images:
                image += "<div><img src='{0}' title='{1}' alt='{1}' /></div>".format(img.image.url, img.title)
            
            new_div = "<div class='gallery' data-id='%s'>%s</div>" % (pk, image) 
            regex3 = re.compile('\<p\>(\[album\]%s\[\/album\])\<\/p\>+'% pk)
            content = re.sub(regex3, new_div, unicode(content))

    return content

@register.filter
def get_video_embed_html(content, replace_parent=False):
    return  get_oembed_html(content).get('html')

@register.assignment_tag(name='get_oembed_object', takes_context=True)
def get_oembed_object(context, item):
    request = context['request']
    BotNames=[
        'Googlebot',
        'Slurp',
        'Twiceler',
        'msnbot',
        'KaloogaBot',
        'YodaoBot',
        '"Baiduspider',
        'googlebot',
        'Speedy Spider',
        'DotBot'
    ]
    user_agent=request.META.get('HTTP_USER_AGENT',None)
    if not user_agent in BotNames:
        return  get_oembed(item)
    return  item

@register.filter
def get_markdown(item):
    renderer = mistune.Renderer(escape=False, hard_wrap=True)
    markdown = mistune.Markdown(renderer=renderer)
    return markdown(item)