from django.contrib.contenttypes import generic
from django.contrib.contenttypes.forms import BaseGenericInlineFormSet
from django.utils.datastructures import MultiValueDictKeyError
from django import forms
from django.forms.models import modelformset_factory
from globaly.models import GlobalyUrls

class MyInlineFormset(BaseGenericInlineFormSet):

    def _construct_form(self, i, **kwargs):
        """
        Override the method to change the form attribute empty_permitted
        """
        form = super(MyInlineFormset, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form

class GlobalyUrlsAdminForm(forms.ModelForm):
    class Meta:  
        exclude = ('view_name','paginated',)

class GlobalyUrlsInline(generic.GenericStackedInline):
    model = GlobalyUrls
    #form = GlobalyUrlsAdminForm
    max_num = 1
    exclude = ['view_name','paginated']
    #formset = MyInlineFormset # it doesnt work in heroku
    def has_delete_permission(self, request, obj=None):
        return False

class GlobalyTagsAdminForm(forms.ModelForm):
    
    widgets = {
        'meta_description':  forms.Textarea(attrs={'maxlength':156,}),
    }

class SearchForm(forms.Form):
    q = forms.CharField(max_length=255)
    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['q'].required = False
