(function ($) 
{
    featured_label = $("#uploaded_images").attr('data-featured-label');
    videos_featured_label = $("#uploaded_videos").attr('data-featured-label');
    videos_description_label = $("#uploaded_videos").attr('data-description-label');
    videos_delete_label = $("#uploaded_videos").attr('data-delete-label');
    var image_id = 0;
    var video_id = 0;
	
    show_page = function(e)
	{
		$("#searchform").css({
			'display':'inline'
			
		});	
	}
	$("#showsearchform").on('click', show_page);
	
    select_date_list_option = function(e)
	{
		$(this).parent().find('li').removeClass('active');
		$(this).addClass('active');
		$(this).parent().parent().find('input').val(
			$(this).attr('data-days')
		);
	}
	$(".days_list li").on('click',select_date_list_option);

	if($.isFunction($.fn.datetimepicker))
	{
		$('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY HH:mm:ss'
        });
	}
 
    var get_images_deleted = function(data)
    {
        if(data.success==true)
        {
            $('.photo[data-id='+data.id+']').remove();            
        }
    }

    var get_images_featured = function(data)
    {
        if(data.success==true)
        {
            if($('.photo[data-id='+data.id+'] .featured').hasClass("btn-success"))
            {
                $('.photo[data-id='+data.id+'] .featured').removeClass("btn-success"); 
                $('.photo[data-id='+data.id+'] .featured').addClass("btn-default"); 
               
            }else
            {
                $('.photo[data-id='+data.id+'] .featured').removeClass("btn-default"); 
                $('.photo[data-id='+data.id+'] .featured').addClass("btn-success");
            }
          
        }
    }

    var delete_image = function (e)
    {
        var id = $(this).parent().attr('data-id');
        var url = $("#uploaded_images").attr('data-delete-url');
        var data = new FormData();
        data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
        data.append('pk', id);
        data.append('GUID', $('#id_GUID').val());
        $.ajax(
        {
            url:  url,
            type: 'POST',
            success: get_images_deleted,
            data: data,            
            cache: false,
            contentType: false,
            processData: false
        });
    }

    var set_featured_image = function (e)
    {
        var id = $(this).parent().attr('data-id');
        var url = $("#uploaded_images").attr('data-featured-url');
        var data = new FormData();
        data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
        data.append('pk', id);
        data.append('GUID', $('#id_GUID').val());
        data.append('featured',  $(this).hasClass('btn-default') ? 1 : 0);
        $.ajax(
        {
            url:  url,
            type: 'POST',
            success: get_images_featured,
            data: data,            
            cache: false,
            contentType: false,
            processData: false
        });
    }

    $('.images .photo .delete').on('click', delete_image); 	
	$('.images .photo .featured').on('click', set_featured_image);    

	select_active_date_list = function(i, val)
	{
		value = $(val).val();
        
        if(value.length > 0)
        {
           $(this).parent().find("li[data-days="+value+"]").addClass('active');
        }
		
	}
	$(".days_list").parent().find('input').each(select_active_date_list);

    var completeHandler = function (data)
    {
        $.each(data.images, function(i,val)
        {                       
            u = $("#uploaded_images").find('.images').find('ul');
            i = u.append('<li><div data-id="'+val.id+'" class="photo" style="background:url('+val.url+');"><i class="delete fa fa-times-circle" aria-hidden="true"></i><button type="button" class="featured btn  btn-default "><span class="fa fa-check" aria-hidden="true">'+featured_label+'</span></button></div></li>');
            i.find('.featured').on('click', set_featured_image);
            i.find('.delete').on('click', delete_image);
        });

    }

    function sendFile(e) 
    {
        var data = new FormData();
        data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
        data.append('GUID', $('#id_GUID').val());
        files = e.target.files; 
		$.each(files,passfiles);
		function passfiles(i,val)
		{
		  data.append('files', val);
		}

        $.ajax(
        {
            url:  $(this).attr('data-url'),  //Server script to process data
            type: 'POST',
            enctype: "multipart/form-data",
            success: completeHandler,
            data: data,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
    }
  	file_field = $('#id_files'); 
    file_field.on('change', sendFile);    

	var open_uploader = function (e)
	{
		e.stopImmediatePropagation();
		$('#id_files').focus().trigger('click');
	}

	$('.draggable').on('click', open_uploader);

    var get_videos_deleted = function(data)
    {
        if(data.success==true)
        {
            $('.video[data-id='+data.id+']').remove();            
        }
    }


    var delete_video = function (e)
    {
        var id = $(this).attr('data-id');
        var url = $("#uploaded_videos").attr('data-delete-url');
        var data = new FormData();
        data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
        data.append('pk', id);
        data.append('GUID', $('#id_GUID').val());
        $.ajax(
        {
            url:  url,
            type: 'POST',
            success: get_videos_deleted,
            data: data,            
            cache: false,
            contentType: false,
            processData: false
        });
    }

    var get_videos_featured = function(data)
    {
        if(data.success==true)
        {
            if($('.video[data-id='+data.id+'] .featured').hasClass("btn-success"))
            {
                $('.video[data-id='+data.id+'] .featured').removeClass("btn-success"); 
                $('.video[data-id='+data.id+'] .featured').addClass("btn-default"); 
               
            }else
            {
                $('.video[data-id='+data.id+'] .featured').removeClass("btn-default"); 
                $('.video[data-id='+data.id+'] .featured').addClass("btn-success");
            }
          
        }
    }

    var set_featured_video = function (e)
    {
        var id = $(this).attr('data-id');
        var url = $("#uploaded_videos").attr('data-featured-url');
        var data = new FormData();
        data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
        data.append('pk', id);
        data.append('GUID', $('#id_GUID').val());
        data.append('featured',  $(this).hasClass('btn-default') ? 1 : 0);
        $.ajax(
        {
            url:  url,
            type: 'POST',
            success: get_videos_featured,
            data: data,            
            cache: false,
            contentType: false,
            processData: false
        });
    }


    var completeVideoHandler = function (data)
    {
        $.each(data.videos, function(i,val)
        {                       
            u = $("#uploaded_videos").find('.videos').find('ul');
            featured_button = '<button type="button" class="featured btn btn-default "  data-id="'+val.id+'" >';
            featured_button += '<span class="fa fa-check" aria-hidden="true"></span> ';
            featured_button += videos_featured_label;
            featured_button += '</button> ';
            description_button = '<button type="button" class="showmodal btn btn-primary" data-id="'+val.id+'" >';
            description_button += '<span class="fa fa-edit" aria-hidden="true"></span> ';
            description_button += videos_description_label;
            description_button += '</button> ';
            delete_button = '<button type="button" class="delete-video btn btn-danger" data-id="'+val.id+'">';
            delete_button += '<i class="delete fa fa-times-circle" aria-hidden="true"></i> ';
            delete_button += videos_delete_label;
            delete_button += '</button> ';
            video_html = '<div class="videoWrapper youtube">'+val.html+'</div>';
            i = u.append('<li class="video" data-id="'+val.id+'">'+video_html+featured_button+description_button+delete_button+'</li>');
            i.find('.featured').on('click', set_featured_video);
            i.find('.delete-video').on('click', delete_video);
        });

    }

    $('.videos .video .delete-video').on('click', delete_video);  
    $('.videos .video .featured').on('click', set_featured_video); 

    var add_video_link = function (e)
    {
        e.preventDefault()
        video_link = $("#video_link").val();
        if (video_link.length > 0)
        {
            var data = new FormData();
            data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
            data.append('GUID', $('#id_GUID').val());
            data.append('link', video_link);
            $.ajax(
            {
                url:  $(this).attr('data-url'),  //Server script to process data
                type: 'POST',
                enctype: "multipart/form-data",
                success: completeVideoHandler,
                data: data,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });           
        }
    }
    $("#add_video_link").on('click', add_video_link);

   //----------------------
    var GUID = function () {
        //------------------
        var S4 = function () 
        {
            return(
                    Math.floor(
                            Math.random() * 0x10000 /* 65536 */
                        ).toString(16)
                );
        };
        //------------------

        return (
                S4() + S4() + "-" +
                S4() + "-" +
                S4() + "-" +
                S4() + "-" +
                S4() + S4() + S4()
            );
    };
    //----------------------

    
    GUID = "GUID-" + GUID();
    $('#id_GUID').val(GUID);
    var showphone = function(e)
    {
        $(this).html($(this).attr('data-phone'));
    }

    $(".showphone").on('click', showphone)

    var openLogoUploader = function(e)
    {
        e.preventDefault();
        $("#id_logo").trigger('click');
    }

    var setLogo = function(data)
    {
        //console.log(data.success)
        if(data.success==true)
        {
            style = {
                "display": "block",
                "background-image": "url("+data.logo+")",
                "background-repeat": "no-repeat",
                "background-position": "center"
            }
            $("#userlogo").css(style);                
        }
    } 

    var uploadProfileImage = function(e)
    {
        var data = new FormData();
        data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
       
        files = e.target.files; 
        $.each(files,passfiles);
        function passfiles(i,val)
        {
          data.append('logo', val);
        }

        $.ajax(
        {
            url:  $(this).attr('data-url'),
            type: 'POST',
            enctype: "multipart/form-data",
            success: setLogo,
            data: data,
            cache: false,
            contentType: false,
            processData: false
        });

    }

    var showdescriptionformmodal = function(e)
    {
        image_id = $(this).attr('data-id');
        //window.myEditorDescription.value('')   
        var get_image_description = function( data ) 
        {   
            $("#description").val(data.description);
            ///window.myEditorDescription.value(data.description)
            window.myEditorDescription.value(data.description);
            $('#myModal').modal('show'); 
        }
        $.getJSON( 
            $(this).attr('data-url')+'/'+$(this).attr('data-id'), 
            get_image_description
        );
               
    }

    var showvideodescriptionformmodal = function(e)
    {
        video_id = $(this).attr('data-id');
        //window.myEditorDescription.value('')   
        var get_video_description = function( data ) 
        {   
            $("#descriptionVideo").val(data.description);
            
            window.myEditorVideoDescription.value(data.description);
            $('#myModalVideo').modal('show'); 
        }
        $.getJSON( 
            $("#uploaded_videos").attr('data-description-url') + '/' + $(this).attr('data-id'), 
            get_video_description
        );
               
    }

    $("#uploaded_videos button.showmodal").click(showvideodescriptionformmodal);
    $("#uploaded_images button.showmodal").click(showdescriptionformmodal);
    $("#openuploader").on('click', openLogoUploader);
    $("#id_logo").on('change', uploadProfileImage)

    var UpdateVideoDescription = function(e)
    {
        var description =  window.myEditorVideoDescription.value();
        console.log("videoDescription:" + description)
        if (video_id != 0 && description.length > 0)
        {
            var data = new FormData();
            data.append('csrfmiddlewaretoken', $.cookie("csrftoken"));
            data.append('pk', video_id);
            data.append('description', description);
            data.append('GUID', $('#id_GUID').val());
            var CleanModal = function(data)
            {
                $("#descriptionVideo").val(null);
                window.myEditorVideoDescription.value('');
                $('#myModalVideo').modal('hide'); 
            }
            $.ajax(
            {
                url:  $("#uploaded_videos").attr('data-description-url'), 
                type: 'POST',
                enctype: "multipart/form-data",
                success: CleanModal,
                data: data,                
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }

    $("#UpdateVideoDescription").on('click', UpdateVideoDescription);

    var UpdateImageDescription = function(e)
    {
        var description =  window.myEditorDescription.value();
       
        if (image_id != 0 && description.length > 0)
        {
            var data = new FormData();
            data.append('csrfmiddlewaretoken',  $.cookie("csrftoken"));
            data.append('pk', image_id);
            data.append('description', description);
            data.append('GUID', $('#id_GUID').val());
            var CleanModal = function(data)
            {
                $("#description").val(null);
                window.myEditorDescription.value('');
                $('#myModal').modal('hide'); 
            }
            $.ajax(
            {
                url:  $("#uploaded_images").attr('data-description-url'), 
                type: 'POST',
                enctype: "multipart/form-data",
                success: CleanModal,
                data: data,                
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }

    $("#UpdateDescription").on('click', UpdateImageDescription);


    if(jQuery.isFunction(jQuery.slick))
    {
        $('.gallery').slick({ 
            dots: false,
            infinite: false,
            speed: 300
        });       
        $('.gallery').find('button ').text("");
    }

    

})(jQuery);