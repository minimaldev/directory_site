from django.conf.urls import patterns
from django.conf.urls import url
from directory import views 
# Create your views here.
urlpatterns = patterns('',
    
   url(
    r'^my/adverts/$', 
    views.DirectoryListView.as_view(), 
    name='directory_my_list'),
   url(
    r'^my/adverts/page/(?P<page>\d+)/{0,1}$$', 
    views.DirectoryListView.as_view(), 
    name='directory_my_list'),

    
   url(
    r'^my/adverts/new/$', 
    views.DirectoryItemCreateView.as_view(), 
    name='directory_create_view_list'),

    
   url(
    r'^my/adverts/(?P<pk>\d+)/delete/$', 
    views.DirectoryItemDelete.as_view(), 
    name='directory_create_view_delete'),

   url(
    r'^my/adverts/(?P<pk>\d+)/edit/$', 
    views.DirectoryItemUpdateView.as_view(), 
    name='directory_create_view_edit'),

)