from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url

from django.contrib.auth import views as auth_views
from user_site import views as user_site_views

urlpatterns = patterns('',
      url(r'^login/$',
          user_site_views.login,
          {'template_name': 'modules/user_site/registration/login.html'},
          name='user_site_auth_login'),

      url(r'^logout/$',
          user_site_views.logout,
          {'template_name': 'modules/user_site/registration/logout.html'},
          name='user_site_auth_logout'),
      
      url(r'^password/reset/$',
          user_site_views.password_reset,
           {'template_name': 'modules/user_site/registration/password_reset_form.html'},
          name='user_site_auth_password_reset'),

      url(r'^password/change/$',
          user_site_views.password_change,
           {'template_name': 'modules/user_site/registration/password_change_form.html'},
          name='user_site_auth_password_change'),
      
      url(r'^password/change/done/$',
          user_site_views.password_change_done,
           {'template_name': 'modules/user_site/registration/password_change_done.html'},
          name='user_site_auth_password_change_done'),

      url(r'^password/reset/$',
          user_site_views.password_reset,
          name='user_site_password_reset'),
      
      url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
          user_site_views.password_reset_confirm,
          name='user_site_auth_password_reset_confirm'),

      url(r'^password/reset/complete/$',
          user_site_views.password_reset_complete,
          name='user_site_auth_password_reset_complete'),
      
      url(r'^password/reset/done/$',
          user_site_views.password_reset_done,
          name='user_site_auth_password_reset_done'),
)
