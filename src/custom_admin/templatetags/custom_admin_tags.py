from django import template
register = template.Library()
from django.apps import apps
from django.conf import settings
from django.utils import six
from django.utils.text import capfirst
from django.utils.translation import ugettext as _, ugettext_lazy
from django.utils.text import capfirst
from django.contrib import admin
from django.contrib.admin.templatetags.admin_list import result_headers, result_hidden_fields, results
from django.core.urlresolvers import NoReverseMatch, reverse
site = admin.site

@register.assignment_tag(name='get_admin_navigation', takes_context=True)
def get_admin_navigation(context):
    request = context['request']
    app_dict = {}
    name = site.name
    for model, model_admin in site._registry.items():
        if not hasattr(model._meta,'show_in_admin_menu'):
            app_label = model._meta.app_label
            has_module_perms = model_admin.has_module_permission(request)

            if has_module_perms:
                perms = model_admin.get_model_perms(request)

                # Check whether user has any perm for this module.
                # If so, add the module to the model_list.
                if True in perms.values():
                    info = (app_label, model._meta.model_name)
                    model_dict = {
                        'name': capfirst(model._meta.verbose_name_plural),
                        'object_name': model._meta.object_name,
                        'perms': perms,
                    }
                    if perms.get('change', False):
                        try:
                            model_dict['admin_url'] = reverse(
                                'admin:%s_%s_changelist' % info, 
                                current_app=name
                            )
                        except NoReverseMatch:
                            pass
                    if perms.get('add', False):
                        try:
                            model_dict['add_url'] = reverse(
                                'admin:%s_%s_add' % info, 
                                current_app=name
                            )
                        except NoReverseMatch:
                            pass
                    if app_label in app_dict:
                        app_dict[app_label]['models'].append(model_dict)
                    else:
                        app_dict[app_label] = {
                            'name': apps.get_app_config(app_label).verbose_name,
                            'app_label': app_label,
                            'app_url': reverse(
                                'admin:app_list',
                                kwargs={'app_label': app_label},
                                current_app=name,
                            ),
                            'has_module_perms': has_module_perms,
                            'models': [model_dict],
                        }

    # Sort the apps alphabetically.
    app_list = list(six.itervalues(app_dict))
    app_list.sort(key=lambda x: x['name'].lower())

    # Sort the models alphabetically within each app.
    for app in app_list:
        app['models'].sort(key=lambda x: x['name'])
    return app_list


@register.inclusion_tag("admin/media/mediaimage/change_list_results.html")
def custom_result_list(cl):
    """
    Displays the headers and data list together
    """
    headers = list(result_headers(cl))
    num_sorted_fields = 0
    for h in headers:
        if h['sortable'] and h['sorted']:
            num_sorted_fields += 1
    return {'cl': cl,
            'result_hidden_fields': list(result_hidden_fields(cl)),
            'result_headers': headers,
            'num_sorted_fields': num_sorted_fields,
            'results': list(results(cl))}

@register.filter()
def custom_result_list_revesed(item):   

    return list(reversed(item))
