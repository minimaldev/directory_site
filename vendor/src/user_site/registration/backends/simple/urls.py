from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url
from django.views.generic.base import TemplateView
from user_site.registration.backends.simple.views import RegistrationView

urlpatterns = patterns('',
	
        url(r'^register/$',
            RegistrationView.as_view(),
            name='user_site_registration_register'),

        url(r'^register/closed/$',
            TemplateView.as_view(template_name='auth/registration/registration_closed.html'),
            name='user_site_registration_disallowed'),

        url(r'', include('registration.auth_urls')),
)
