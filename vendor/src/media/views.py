from django.http import Http404, HttpResponseRedirect, HttpResponse
import json
from utilities.media import get_oembed_html

def oembed_html(request):
	context = {'html':{}}
	link = request.GET.get('link')
	if link:
		html =  get_oembed_html(link).get('html')
		if html is not None:
			context['html'] = html
	return HttpResponse(json.dumps(context), content_type="application/json")
