from src.user_site.models import UserSite
class FacebookBackend:
    
    def authenticate(self, profile=None):
        return profile
   
    def get_user(self, user_id):

        try:
            return UserSite.objects.get(pk=user_id)
        except UserSite.DoesNotExist:
            return None