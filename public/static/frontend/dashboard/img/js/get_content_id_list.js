(function($)
{ 
	$(function()
	{
		$(document).ready(function()
		{

			function UpdateItems(data)
			{
				var elem = $("#id_object_id");
			 	elem.empty();
			 	if (data.items.length > 0)
			 	{				 		
			 		var create_item = function (i,val)
			 		{
			 			option = "<option value="+val.id+" >"+val.name+"</option>";
			 			elem.append(option);
			 		}
			 		$.each(data.items, create_item);
			 	}
			 	else
			 	{
			 		option = "<option>---------</option>";
			 		elem.append(option);
			 	}
			}

		  	$("#id_content_type").change(function()
		  	{	 
		  		$.getJSON(
		  			$(this).attr("data-url") + "navigation/item/get/",
		  			{id:$(this).val()},
		  			UpdateItems
		  		);
		 	});
		});
	});
})(django.jQuery);