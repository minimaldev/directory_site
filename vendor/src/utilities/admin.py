from django.contrib import admin
def create_modeladmin(modeladmin, model, name=None):

    def get_model_title():
        if hasattr(modeladmin, 'Meta'):
            if hasattr(modeladmin, 'verbose_name') is None:
                return model._meta.verbose_name
            else:
                return modeladmin.Meta.verbose_name
        else:
            return model._meta.verbose_name

    def get_verbose_name_plural():
        if hasattr(modeladmin, 'Meta') :
            if hasattr(modeladmin, 'verbose_name_plural') is None:
                return model._meta.verbose_name_plural
            else:
                return modeladmin.Meta.verbose_name_plural
        else:
            return model._meta.verbose_name_plural

    class Meta:
        proxy = True
        app_label = model._meta.app_label
        verbose_name = get_model_title()
        verbose_name_plural = get_verbose_name_plural()

    attrs = {'__module__':"", 'Meta': Meta}

    newmodel = type(name, (model,), attrs)
    
    admin.site.register(newmodel, modeladmin)
    return modeladmin