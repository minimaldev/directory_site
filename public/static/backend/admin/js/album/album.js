(function($)
{
	if(window.opener)
	{
		
		$(window).load(function()
		{
			var isPopup = function()
			{
			    return window.location.href.search("[?&]_popup=1") != -1;
			};
			
			function cancelpress(event)
			{
				event.preventDefault();
				//var selector_id = "#id_"+window.opener.selector_id
				var id = $(this).find('span').attr('data-id');
				window.opener.myEditor.codemirror.replaceSelection('[album]'+id+'[/album]\n\n')	               
				window.close();

			}
			$(".field-album_title a").on('click', cancelpress)
		});	
		
	}


 
})(django.jQuery);