(function($)
{ 
	$(window).load(function()
	{
		var isPopup = function()
		{
		    return window.location.href.search("[?&]_popup=1") != -1;
		};

		if (isPopup()==true)
		{
			$(".selectItem").css('cursor','pointer');
			$(".column-alt_text,.field-alt_text").remove();
		}
		
		function cancelpress(event)
		{
			event.preventDefault();
			var selector_id = "#id_"+window.opener.selector_id
			var elem = window.opener.$(selector_id);
			var image_url = $(this).attr('data-src');
			var alt_text = $(this).attr('data-title');
			    if (elem.is("input")) 
			    {
			    	elem.val(image_url);
			    	window.opener.$(selector_id+"_text").val(alt_text);
			    }
			    else if (elem.is("textarea")) 
			    {
			        tinymce = window.opener.tinymce.activeEditor;	
			        tinymce.execCommand('mceInsertContent',false,'<img data-mce-alt="'+alt_text+'" data-mce-src="'+image_url+'" alt="'+alt_text+'" src="'+image_url+'" />');	        
			    }    	
			window.close();
		}
		$(".selectItem").on('click', cancelpress)
	});	
 
})(django.jQuery);