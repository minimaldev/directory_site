# -*- coding: utf-8 -*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from utilities.image_base64 import encode_image_3
from utilities.image_base64 import encode_image_2
from utilities.models import BaseDateTime
from utilities.models import BaseArticle
from utilities.models import BaseThumbnailFeatured
from utilities.models import BasePublish
from utilities.models import BaseSeo
from globaly.models import GlobalyTags
from globaly.models import get_meta
from media.models import MediaAlbum
from django.db import connection
from django.utils import timezone
from django.db.models import Count
from django.conf import settings
from django.apps import apps
from user_site.models import UserSite

ITEMS_CHOICES = (
    ('services', _('SERVICES_LABEL')),
    ('articles', _('ARTICLES_LABEL')),
    ('turims', _('TOURISM_LABEL')),
    ('properties', _('PROPERTIES_LABEL')),
    ('vehicles', _('VEHICLES_LABEL')),
    ('jobs', _('JOBS_LABEL')),
)


class DirectoryCategory(MPTTModel, BaseDateTime, BasePublish, BaseSeo):

    parent = TreeForeignKey(
            'self',
            verbose_name=_('PARENT_LABEL'),
            null=True,
            blank=True,
            on_delete=models.CASCADE,
            related_name='directory_categories_parent'
        )
    name = models.CharField(
            _('NAME_LABEL'),
            max_length=255,
            unique=True,
            blank=True      
        )

    
    def view_name(self):
        return "category"
    
    view_name = property (view_name)
    
    def is_paginated(self):
        return True
    
    is_paginated = property (is_paginated)
    
    def title_name(self):
        return "%s - %s" % (
            self.get_directory_type(), 
            self.name
        )
    
    def get_directory_type(self):
        directory_types = {
           "directory":_('POST_CATEGORY_ARTICLE_TITLE'), 
           }
        return directory_types.get("directory")

    @staticmethod
    def get_items_navigation():
        return DirectoryCategory.objects.all().order_by('-id')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('DIRECTORY_CATEGORY_TITLE')
        verbose_name_plural = _('DIRECTORY_CATEGORY_TITLE_PLURAL')
        get_latest_by = 'created'
        ordering = ('-id',)
        db_table = 'directory_categories'
        app_label = 'directory'


class DirectoryItem(BaseArticle, BaseDateTime, BaseThumbnailFeatured):

    autor = models.ForeignKey(
            UserSite,
            verbose_name=_('AUTOR_LABEL'),
            null=True,
            blank=True,
            on_delete=models.CASCADE,
            related_name='directory_item_autor'
        )

    album = models.ManyToManyField(
            MediaAlbum,
            verbose_name=_('MEDIA_ALBUM_TITLE_PLURAL'),
            related_name='directory_item_related_albums',
            blank=True
        )

#    categories = models.ForeignKey(
#            DirectoryCategory,
#            verbose_name=_('BLOG_CATEGORY_TITLE_PLURAL'),
#            related_name='directory_item_related_categories',
#            blank=True
#        )


    categories = models.CharField(
            _('NAME_LABEL'),
            max_length=255,            
            choices=ITEMS_CHOICES,
            blank=True      
        )

    tags = models.ManyToManyField(
            GlobalyTags,
            verbose_name=_('GLOBALY_TAG_TITLE_PLURAL'),
            related_name='directory_item_related_tags', 
            blank=True
        )
    
    publish_date = models.DateTimeField(
            _('Publish Date'), 
            default=timezone.now
        )
    release_date = models.DateTimeField(
            _('Release Date'), 
            default=timezone.now
        )
    featured_start_date = models.DateTimeField(
            _('Featured Start Date'), 
            null=True, 
            blank=True
        )
    featured_end_date = models.DateTimeField(
            _('Featured End Date'), 
            null=True, 
            blank=True
        )   
    featured_days = models.IntegerField(
            _('Hits'), 
            null=True, 
            blank=True,
        )   
    release_date_days = models.IntegerField(
            _('Hits'), 
            null=True, 
            blank=True,
        )   
    price = models.FloatField(
            _('PRICE_LABEL'), 
            null=True, 
            blank=True
        )   

    phone = models.CharField(
            _('PHONE_LABEL'), 
            null=True, 
            blank=True,
            max_length=15
        ) 
    hits = models.IntegerField(
            _('Hits'), 
            null=True, 
            blank=True,
        )     
    def __unicode__(self):
        return self.title

    def view_name(self):
        return "directory_details"

    view_name = property (view_name)
    
    def is_paginated(self):
        return True
    
    def __init__(self, *args, **kwargs):
        super(DirectoryItem, self).__init__(*args, **kwargs)
        if self.release_date_days is not None:
            self._release_date_days = self.release_date_days
        if self.featured_days is not None:
            self._featured_days = self.featured_days
      

    
    is_paginated = property (is_paginated)

    def title_name(self):
        return "%s - %s" % (
            self.get_directory_type(), 
            self.title
        )
    
    @staticmethod
    def get_items_navigation():
        return DirectoryItem.objects.all().order_by('-id')
    
    def thumb(self):
        thumb =  self.get_thumb()
        if thumb is not None:
            return format_html(
                    '<img src="data:image/png;base64,{0}" />',
                    thumb
                )
        else:
            return "posts"
    thumb.allow_tags = True

    def get_thumb(self):
        thumbnail = self.thumbnail
        if len(thumbnail) > 0:
            
            return encode_image_3(thumbnail)
        return None

    def Tags(self):
        return ",".join([t.name for t in self.tags.all()])

    def Categories(self):
        return ",".join([c.name for c in self.categories.all()])
    
    @staticmethod
    def SitemapToMonth():
        posts = PostItem.objects.filter(publish=True).\
            extra(
                select={
                    'year': "EXTRACT(year FROM created)",
                    'month': "EXTRACT(month FROM created)",
                    'year_month': "EXTRACT(YEAR_MONTH FROM created )"
                }).\
            values('year', 'month', 'year_month').\
            annotate(total=Count('id')).\
            order_by('year_month')
        return posts
    
    @staticmethod
    def SitemapToMonthRawSQl():
        def dictfetchall(cursor):
            "Return all rows from a cursor as a dict"
            desc = cursor.description
            return [
                dict(zip([col[0] for col in desc], row))
                for row in cursor.fetchall()
            ]   
        current_date =  timezone.now()     
        sql = """
                
                SELECT 
                (
                    SELECT created FROM directory_items 
                    where DATE_FORMAT(created, '%Y%m') = DATE_FORMAT(A.created, '%Y%m') 
                    ORDER BY id desc limit 1
                ) as created,
                DATE_FORMAT(A.created, '%Y') as year,
                DATE_FORMAT(A.created, '%m') as month,
                'post-items' as title,
                COUNT(A.id) as total
                FROM directory_items as A
                where A.publish = 1  and (A.publish_date IS NULL or A.publish_date <= NOW()) 
                GROUP BY DATE_FORMAT(A.created, '%Y%m')
            """
        # 
        cursor = connection.cursor()
        cursor.execute(sql)
        data = dictfetchall(cursor)
        cursor.close()
        return data

    @staticmethod
    def SitemapPerYearMonth( year, month):
        return PostItem.objects.filter(publish=True).\
            extra(
                where=[
                    "EXTRACT(year FROM created) = %s ",
                    "EXTRACT(month FROM created) = %s "
                ], 
                params=[year, month]
            )

    def get_directory_type(self):
        directory_types = {
           "directory_item":_('DIRECTORY_TITLE'), 
           }
        return directory_types.get('directory_item')
    
    class Meta:
        verbose_name = _('DIRECTORY_ITEM_TITLE')
        verbose_name_plural = _('DIRECTORY_ITEM_TITLE_PLURAL')
        get_latest_by = 'created'
        ordering = ('-id',)
        db_table = 'directory_item'
        app_label = 'directory'

