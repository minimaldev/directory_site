from django.db.models import Q
from django.db.models import Max
from django.template.response import TemplateResponse
from utilities.paginator import paginator
from user_site.decorators import login_required
from django.utils.decorators import method_decorator
from user_site.models import UserSite
from directory.models import DirectoryCategory
from directory.models import DirectoryItem
from globaly.models import GlobalyTags
from directory.forms import SearchForm
from directory.forms import DirectoryItemForm
from django.shortcuts import get_object_or_404
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.utils import timezone
import  datetime
from decimal import Decimal 

def search(request,slug=None,page=1):

    context = {}
    current_date =  timezone.now()
    context['search_word'] = request.GET.get('q', '')
    context['min'] = Decimal(request.GET.get('min', 0))
    context['max'] = Decimal(request.GET.get('max', 0))
    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q()
    if slug is not None:
       
        f4 = Q(categories=slug)    
    
    f5 = Q(title__icontains=context['search_word'])
    f6 = Q()
    if not context['max'] or context['max'] <= 0:
        context['max'] = DirectoryItem.objects.aggregate(Max('price'))['price__max']   
    
    f6 = Q(price__range=(context['min'], context['max']))
    context['posts'] = paginator(
                page, 
                DirectoryItem.objects.filter(
                    f1 & (f2 | f3) & (f5) & f4 & f6
                ).order_by("-publish_date",'-id')
            )
    
    template_name = [
            'frontend/modules/directory/search.html'
        ]
    return TemplateResponse(request, template_name, context)


def choicess(request, slug=None, page=1):

    context = {}
    current_date =  timezone.now()
    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(categories=slug) 

    context['category'] = slug
    context['slug'] = slug    
    context['posts'] = paginator(
                page, 
                DirectoryItem.objects.filter(
                    f1 & (f2 | f3) & (f4)
                ).order_by("-publish_date",'-id')
            )
    
    
    template_name = [           
            'frontend/modules/directory/category-%s.html' % ( slug),
            'frontend/modules/directory/category.html'
        ]
    return TemplateResponse(request, template_name, context)



def category(request, slug=None, page=1, model=None):

    context = {}
    current_date =  timezone.now()

    f1 = Q(publish=True)
    f6 = Q(slug=slug)

    if model is None:
        model = get_object_or_404(DirectoryCategory, f1 & f6 )
    
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(categories__id=model.id) 

    context['category'] = model
    context['slug'] = slug    
    context['posts'] = paginator(
                page, 
                DirectoryItem.objects.filter(
                    f1 & (f2 | f3) & (f4)
                ).order_by("-publish_date",'-id')
            )
    
    
    template_name = [
            'frontend/modules/directory/category-%d.html' % ( model.id),
            'frontend/modules/directory/category-%s.html' % ( slug),
            'frontend/modules/directory/category.html'
        ]
    return TemplateResponse(request, template_name, context)

def post_details(request, slug=None, model=None):
    
    context = {}
    current_date =  timezone.now()
    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None) 
    f4 = Q(id=slug)

    if model is None:
        model = get_object_or_404(DirectoryItem, f1 & (f2 | f3) & f4  )
    
    context['post'] = model
    #context['related_posts'] = context['post'].related_posts.filter( 
    #        f1 & (f2 | f3) 
    #    ).order_by("-publish_date",'-id')

    context['tags'] = context['post'].tags.all()
   
    viewed_adverts = request.session.get('viewed_adverts',{})

    if bool(viewed_adverts) == False:
 
        request.session['viewed_adverts'] = {'dt':current_date, 'items':[int(model.id)]}

    if bool(viewed_adverts) == True:
        items = viewed_adverts.get('items')
        
        if int(model.id) not in items:
            items.append(int(model.id)) 
            request.session['viewed_adverts'] = {'dt':viewed_adverts.get('dt') , 'items':items}
            print request.session['viewed_adverts'] 

    if bool(viewed_adverts) == True:
        expired = viewed_adverts.get('dt', None)
   
        MAX_AGE = datetime.timedelta(minutes=5)
        
        if expired is not None:
            if current_date - expired  > MAX_AGE:
                del request.session['viewed_adverts']

    template_name =  [
            'frontend/modules/directory/post-%d.html' % ( model.id),
            'frontend/modules/directory/post-%s.html' % ( slug),
            'frontend/modules/directory/post.html',
            
        ] 
    return TemplateResponse(request, template_name, context)

def autor(request, slug=None, page=1, model=None):

    context = {}
    current_date =  timezone.now()

    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(autor__nick=slug)    
    

    post_type = "post"

    context['autor'] = slug

    context['posts'] = paginator(
                page, 
                DirectoryItem.objects.filter(
                    f1 & (f2 | f3) & f4 
                ).order_by("-publish_date",'-id')
                
            )

    template_name = [
            #'modules/%s/autor-%d.html' % (post_type, model.id),
            'frontend/modules/directory/autor-%s.html' % ( context['autor']),
            'frontend/modules/directory/autor.html'
        ]
    return TemplateResponse(request, template_name, context)

def tags(request, slug=None, page=1, model=None):

    context = {}
    current_date =  timezone.now()

    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(tags__slug=slug)
    
    context['tag'] = get_object_or_404( GlobalyTags,slug=slug)
    context['posts'] = paginator(
                page, 
                DirectoryItem.objects.filter(
                    f1 & (f2 | f3) & f4
                ).order_by("-publish_date",'-id')
            )

    template_name = [
            #'modules/%s/autor-%d.html' % (post_type, model.id),
            'frontend/modules/directory/tag-%s.html' % (context['tag']),
            'frontend/modules/directory/tag.html' 
        ]
    return TemplateResponse(request, template_name, context)


class DirectoryListView(ListView):
    
    model = DirectoryItem
    template_name = 'dashboard/modules/directory/user_site/list.html'

    def get_queryset(self):
        return DirectoryItem.objects.filter(
            autor=self.request.user_site
        )

    def get_context_data(self, **kwargs):
        context = super(DirectoryListView, self).get_context_data(**kwargs) 
        context['search_word'] = self.request.GET.get('q', '')
        f5 = Q(title__contains=context['search_word'])
        f4 = Q(autor=self.request.user_site)
        list_exam = DirectoryItem.objects.filter(
           f4 & f5
        ).order_by('-id')
        list2 = DirectoryItem.objects.filter(
           f4
        )
        page =  self.kwargs.get('page', 1)
        
        context['has_adverts'] = len(list2) == 0 or len(list_exam) > 0
        context['directoryitem_list'] = paginator(page, list_exam)
        return context

    @method_decorator(login_required)  
    def dispatch(self, *args, **kwargs):
        return super(DirectoryListView, self).dispatch(*args, **kwargs) 

class DirectoryItemDetailView(DetailView):

    model = DirectoryItem
    template_name = 'dashboard/modules/directory/user_site/detail.html'

    def get_queryset(self):
        return DirectoryItem.objects.get(
            autor=self.request.user_site,
            pk=self.kwargs['pk']
        )

    @method_decorator(login_required) 
    def dispatch(self, *args, **kwargs):
        return super(DirectoryItemDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DirectoryItemDetailView, self).get_context_data(**kwargs) 
        return context

class DirectoryItemUpdateView(UpdateView):

    template_name = 'dashboard/modules/directory/user_site/edit.html'
    model = DirectoryItem
    form_class = DirectoryItemForm
    success_url = reverse_lazy('directory_my_list')
    
    def get_queryset(self):
        return DirectoryItem.objects.filter(
            autor=self.request.user_site,
            pk=self.kwargs['pk']
        )

    def get_success_url(self):
        return reverse_lazy(
            'directory_create_view_edit', 
            kwargs={'pk': self.kwargs['pk']}
        )
    
    def get_form_kwargs(self):
        # pass "user" keyword argument with the current user to your form
        kwargs = super(DirectoryItemUpdateView, self).get_form_kwargs()

        kwargs['user_site'] = self.request.user_site
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        form.instance.autor = self.request.user_site   
        form._request = self.request
        form._is_new = False
        return super(DirectoryItemUpdateView, self).form_valid(form)
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DirectoryItemUpdateView, self).dispatch(*args, **kwargs) 

class DirectoryItemCreateView(CreateView):

    template_name = 'dashboard/modules/directory/user_site/edit.html'
    model = DirectoryItem
    form_class = DirectoryItemForm
    success_url = reverse_lazy('directory_my_list')

    def get_form_kwargs(self):
        # pass "user" keyword argument with the current user to your form
        kwargs = super(DirectoryItemCreateView, self).get_form_kwargs()
        kwargs['user_site'] = self.request.user_site
        kwargs['request'] = self.request
        return kwargs
    
    def form_valid(self, form):
        form.instance.autor = self.request.user_site   
        form._request = self.request
        form._is_new = True
        return super(DirectoryItemCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DirectoryItemCreateView, self).dispatch(*args, **kwargs) 

class DirectoryItemDelete(DeleteView):
    model = DirectoryItem
    template_name = 'dashboard/modules/directory/user_site/delete.html'
    success_url = reverse_lazy('directory_my_list')
    def get_queryset(self):
        return DirectoryItem.objects.filter(
            autor=self.request.user_site,
            pk=self.kwargs['pk']
        )
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DirectoryItemDelete, self).dispatch(*args, **kwargs) 

def public_profile(request, username,  page=1):
    template_name = 'frontend/modules/user_site/public_profile.html'
    context = {}
    context['profile'] = get_object_or_404(
        UserSite, 
        is_active=True, 
        nick=username
    )
    
    context['posts'] = paginator(page, 
            DirectoryItem.objects.filter(
                   autor=context['profile']
                )
            )

    return TemplateResponse(request, template_name, context)
