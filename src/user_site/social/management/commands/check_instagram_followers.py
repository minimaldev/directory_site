from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from django.db import connection
from django.db.models import Count
from src.user_site.models import UserSite
from django.conf import settings
import time
import twitter

from instagram.client import InstagramAPI

#supervisorctl stop chatwhatsapp:social_store_worker_ws
class Command(BaseCommand):
    help = ''
    def handle(self, *args, **options):

         
        for user in UserSite.objects.filter(is_active=True):
            if user.instagram_token is not None \
            and len(user.instagram_token) > 0:
                api = InstagramAPI(access_token=user.instagram_token)
                user_followers, next_ = api.user_followed_by() 
                while next_:
                    more_user_followers, next_ = api.user_followed_by(with_next_url=next_) #this will get you all your followers
                    user_followers.extend(more_user_followers)
                
                UserSite.objects.filter(
                    id=user.id
                ).update(
                    instagram_followers=len(user_followers)
                )