# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from contact_form.models import Contact

class ContactsFormAdmin(admin.ModelAdmin):
    list_display = ('title', 'name','email', 'created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [ 'title', 'name','email','body']}),
    ]

admin.site.register(Contact, ContactsFormAdmin) 