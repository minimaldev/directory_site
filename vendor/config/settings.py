import os, sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
# append src folder and fixing some import errors
sys.path.insert(1,os.path.join(BASE_DIR, 'src')) 

from django.conf import settings

SECRET_KEY = 'nv-=_gl5d(^vd05=9waomq%n_m%=(j98zesf^@$$@erz7-k=j5'

DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '198.167.140.205','www.aprenderunrealengine.com','aprenderunrealengine.com']

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_markup',
    'captcha',
    'widget_tweaks',
    'mptt',
    'django_mptt_admin',
    'form_utils',
    'user',
    'rss',
    'sitemap',
    'utilities',   
    'globaly',   
    'posts',
    'media',
    'global_config',
    'navigation',
    'contact_form',
    'comments',   
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'globaly.middleware.SearchMiddleware',
    'posts.middleware.BlogUrlsMiddleware',
    'navigation.middleware.UrlsMiddleware',
    
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates/default/'),
            os.path.join(BASE_DIR, 'templates/backend/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'sitemap.context_processors.get_domain',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
               
            ],
            #'loaders': [
            #     'django.template.loaders.filesystem.Loader',
            #     'django.template.loaders.app_directories.Loader',
            # ]

            
        },
    },
]

STATICFILES_FINDERS = ("django.contrib.staticfiles.finders.FileSystemFinder",
                       "django.contrib.staticfiles.finders.AppDirectoriesFinder",
                       )

WSGI_APPLICATION = 'config.wsgi.application'

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Caracas'

APPEND_SLASH = True

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', u'English'),
    ('es', u'Spanish'),
)

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'config/locale/')
]

DEFAUT_THEME = 'default'

MEDIA_URL = '/assets/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'public/content/media/')

MEDIA_UPLOAD_ROOT = os.path.join(BASE_DIR, 'public/content/media/uploads/')

STATIC_URL = '/assets/'

STATIC_ROOT = os.path.join(BASE_DIR, 'public/static/')

PUBLIC_DIR = os.path.join(BASE_DIR, 'public')

STATICFILES_DIRS = (
#    os.path.join(BASE_DIR, 'public/static'),
    os.path.join(BASE_DIR, 'public/static/assets'),
    os.path.join(BASE_DIR, 'public/content/'),
)

STATIC_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


try:
    from local_settings import *
except ImportError:
    pass

DATABASES = {
    'default': {
        #unitylatino_django
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'unitylatino_django',
        'USER': 'root',
        'PASSWORD': 'demo',
    }
}

AUTHENTICATION_BACKENDS = ( 

    'django.contrib.auth.backends.ModelBackend',    
    'user.backends.CustomUserBackend', 
)

#cd /home/unitylatino & source venv/bin/activate & pip install -r requirements.txt
#
AUTH_USER_MODEL = 'user.CustomUser'

RECAPTCHA_PUBLIC_KEY = '6LdB3RMTAAAAAI8KOCSQFrS9PnSim4H-d7mDQq99'
RECAPTCHA_PRIVATE_KEY = '6LdB3RMTAAAAAGzpNezah55TZwtOqCSApJJ_bcDy'
#CAPTCHA_AJAX = True
NOCAPTCHA = True
RECAPTCHA_USE_SSL = True