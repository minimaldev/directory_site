from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from utilities.generic.base import TemplateView
admin.autodiscover()



urlpatterns = [
    # Examples:

 

    url(r'^admin/', include("navigation.urls")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^sitemap/', include("sitemap.urls")),
    url(r'^mediaframework/', include("media.urls")),
    url(r'^comments/', include("comments.urls")),
    url(r'^', include("globaly.urls")),
    url(r'^', include("rss.urls")),
    url(r'^$',
        TemplateView.as_view(
            template_name='index.html', 
            content_type='text/html'
        ), 
        name='home'
    ), 
    url(r'^(?P<page>\d+)/{0,1}$',
        TemplateView.as_view(
            template_name='index.html', 
            content_type='text/html'
        ), 
        name='home'
    ), 

    url(r'', include('posts.urls')),
    
    url(r'^(?P<slug>[-\w\d]+)/(?P<page>\d+)/{0,1}$',
        TemplateView.as_view(
         template_name='index.html', 
         content_type='text/html'
        ), 
        name='page_details'
    ),

    url(r'^(?P<slug>[-\w\d]+)/{0,1}$',
        TemplateView.as_view(
         template_name='index.html', 
         content_type='text/html'
        ), 
        name='page_details'
    ),

    url(r'^robots\.txt$', 
        TemplateView.as_view(
            template_name='default/robots.txt', 
            content_type= 'text/plain'
        ),
        name='robots'
    ),
  
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = "globaly.views.handler404"
handler500 = "globaly.views.handler500"
