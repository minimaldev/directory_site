from django.utils.safestring import mark_safe
from django.forms import widgets
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.translation import ugettext_lazy as _
from django.forms.utils import flatatt
from django.utils.html import  format_html


class CustomSelectWidget(widgets.Select):
    def render(self, name, value, attrs=None, choices=()):
        if value is None:
            value = ''
        attrs.update({"data-url":reverse("admin:index")})
        final_attrs = self.build_attrs(attrs, name=name)
        output = [format_html('<select{}>', flatatt(final_attrs))]
        options = self.render_options(choices, [value])
        if options:
            output.append(options)
        output.append('</select>')
        return mark_safe('\n'.join(output))