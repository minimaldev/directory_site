from django.conf.urls import patterns
from django.conf.urls import url
from media import views 
# Create your views here.
urlpatterns = patterns('',

    url(r'oembed/html/{0,1}$',
        views.oembed_html,     
        name='media_oembed_html'
    )
)