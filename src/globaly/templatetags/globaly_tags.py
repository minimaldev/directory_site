from __future__ import division
from django.conf import settings
from django import template
from globaly.models import GlobalyUrls

register = template.Library()

@register.assignment_tag(name='get_nav')
def get_nav():
    return GlobalyUrls.objects.filter(is_nav=True)
