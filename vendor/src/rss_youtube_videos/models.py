#encoding:iso-8859-1
from django.db import models
from django.contrib.auth.models import User
import datetime
from django.utils.timezone import utc
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import ugettext_lazy as _


class VideoChannel(models.Model):

    related_channels = models.ManyToManyField(
            'self', 
            null=True, 
            blank=True, 
            verbose_name=_('Related Channels')
        )

    upload_playlist_id = models.TextField(_('Upload playlist id'), null=True, blank=True)
    have_username = models.BooleanField(_('Is Youtuber'),default=False)
    channel_username = models.TextField(_('Channel Username'),  blank=True)
    visit_per_week = models.CharField(
            _('How many days per Week'), 
            max_length=2,
            choices=((str(x), str(x)) for x in range(1,8)),
            null=True, blank=True
        )
    visit_hour = models.CharField(
            _('Visit this channel at'), 
            max_length=20,
            null=True, blank=True
        )
    las_api_key_used = models.TextField(_('Upload playlist id'), null=True, blank=True)


    visit_counter = models.IntegerField(
            _('Visit Counter'), 
            blank=True,
            default=0,
            null=True
        )
    
    last_visit_date = models.DateTimeField(
            _('Last Visit Date'), 
            blank=True, 
            null=True
        )
    next_visit_date = models.DateTimeField(
            _('Next Visit Date'),
            blank=True,
            null=True
        )   
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)
    
    def __unicode__(self):
        return '%d  - %s' % (self.id, self.channel_username)

    class Meta:
        verbose_name = _('Channels Video')
        verbose_name_plural = _('Videos Channels' )
        get_latest_by = "created"
        ordering = ('-id',)
        db_table = 'rss_youtube_videos_channel'
        app_label = 'rss_youtube_videos'


class VideoChannelVisits(models.Model):
    channel = models.ForeignKey(
            VideoChannel,
            verbose_name=_('User autor'), 
            related_name='rss_youtube_videos_channel_visits_item_autor', 
            null=True,
            blank=True
        )
    last_visit_date = models.DateTimeField(
            _('Last Visit Date'), 
            blank=True, 
            null=True
        )
    next_visit_date = models.DateTimeField(
            _('Next Visit Date'),
            blank=True,
            null=True
        )   
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)
    
    def __unicode__(self):
        return '%d  - %s' % (self.id, self.channel_username)

    class Meta:
        verbose_name = _('Channels Video')
        verbose_name_plural = _('Videos Channels' )
        get_latest_by = "created"
        ordering = ('-id',)
        db_table = 'rss_youtube_videos_channel_visits'
        app_label = 'rss_youtube_videos'



class VideoItem(models.Model):

    channel = models.ForeignKey(
            VideoChannel,
            verbose_name=_('User autor'), 
            related_name='rss_youtube_videos_video_item_autor', 
            null=True,
            blank=True
        )
    related_videos= models.ManyToManyField(
            'self', 
            null=True, 
            blank=True, 
            verbose_name=_('Related Videos')
        )
    link = models.CharField(_('Link'), max_length=255, unique=True)
    title = models.CharField(_('Title'), max_length=255, unique=True)
    is_in_feed = models.BooleanField(_('On Feed'), default=True)
    thumb = models.TextField(_('Thumbnail'), blank=True, null=True)
    excerpt = models.TextField(_('excerpt'), max_length=512, blank=True)
    slug = models.SlugField(
            _('Slug'), 
            max_length=255, 
            unique=True,
            blank=True
        )
    metatitle =  models.CharField(
            _('Meta Title'), 
            max_length=255,
            unique=True,
            blank=True
        )
    metadescription = models.TextField(
            _('Meta Description'),
            blank=True
        )
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)
    
    def __unicode__(self):
        return '%d - %s' % (self.id, self.title)

    class Meta:
        verbose_name = _('Video')
        verbose_name_plural = _('Videos')
        get_latest_by = "created"
        ordering = ('-id',)
        db_table = 'rss_youtube_videos_video_item'
        app_label = 'rss_youtube_videos'