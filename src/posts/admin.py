# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin import RelatedFieldListFilter, RelatedOnlyFieldListFilter

from django.utils.translation import ugettext_lazy as _
from posts.models import PostCategory
from posts.models import PostItem
from posts.forms import PostItemAdminForm
from posts.forms import PostArticleAdminForm
from posts.forms import PostPageAdminForm
from posts.forms import PostVideoAdminForm
from posts.forms import PostCategoryAdminArticleForm
from posts.forms import PostCategoryAdminPageForm
from posts.forms import PostCategoryAdminVideoForm
from utilities.admin import create_modeladmin
from django.conf import settings

class PostCategoryAdmin(admin.ModelAdmin):
    class Media:
        js = (settings.STATIC_URL + '/admin/copytitle.js',)
    list_display = ('name', 'created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
            'parent',
            'name',
            'publish'
        ]}),
        (_('BASIC_SEO'), {'fields': [
            'slug',
            'meta_title',
            'meta_description',
        ]}),     
    ]
    prepopulated_fields = {"slug": ("name",),}

class PostCategoryArticleAdmin(PostCategoryAdmin):
    class Meta:
        verbose_name = _('POST_CATEGORY_ARTICLE_TITLE')
        verbose_name_plural = _('POST_CATEGORY_ARTICLE_TITLE_PLURAL')
    
    form = PostCategoryAdminArticleForm
    
    def get_queryset(self, request):
        return self.model.objects.filter(post_type='post')
    
    def save_model(self, request, obj, form, change):
        # custom stuff here
        if not change:
            obj.post_type = "post"       
        obj.save()

class PostCategoryPageAdmin(PostCategoryAdmin):
    class Meta:
        verbose_name = _('POST_CATEGORY_PAGE_TITLE')
        verbose_name_plural = _('POST_CATEGORY_PAGE_TITLE_PLURAL')        
    
    form = PostCategoryAdminPageForm
    
    def get_queryset(self, request):
        return self.model.objects.filter(post_type='page')

    def save_model(self, request, obj, form, change):
        # custom stuff here
        if not change:
            obj.post_type = "page"       
        obj.save()

class PostCategoryVideoAdmin(PostCategoryAdmin):
    class Meta:
        verbose_name = _('POST_CATEGORY_VIDEO_TITLE')
        verbose_name_plural = _('POST_CATEGORY_VIDEO_TITLE_PLURAL')   
    
    form = PostCategoryAdminVideoForm

    def get_queryset(self, request):
        return self.model.objects.filter(post_type='video')
    
    def save_model(self, request, obj, form, change):
        # custom stuff here
        if not change:
            obj.post_type = "video"
        obj.save()


class PostItemAdmin(admin.ModelAdmin):
    class Media:
        js = (
            #'//cdn.jsdelivr.net/simplemde/latest/simplemde.min.js',
            settings.STATIC_URL + 'js/vendor/simplemde.min.js',
            settings.STATIC_URL + 'js/textareas.js',
            settings.STATIC_URL + 'admin/js/mediamanager/mediamanager_field.js',
            settings.STATIC_URL + 'admin/js/copytitle.js',
            settings.STATIC_URL + 'admin/js/album/album_field.js',
        )
        css = {
             'all': (
                '//cdn.jsdelivr.net/simplemde/latest/simplemde.min.css',
                'https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
                #settings.STATIC_URL + 'frontend/css/simplemde.css',
                )
        }  
    list_filter = (
        ('autor', RelatedOnlyFieldListFilter),
        ('categories', RelatedOnlyFieldListFilter),
        'publish',
        # ...
    )        
    list_display = [
            'title', 'thumb', 'Tags', 
            'Categories','autor' , 'created', 'modified',
            'id',
        ]
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
                'title',
                'content',
                'excerpt',
            ]}),
        (_('PUBLISHING_LABEL'), {'fields': [
                'publish',
                'publish_date',
                'is_featured',
                'featured_start_date',
                'featured_end_date',
                'is_on_feed',
            ]}),
        (_('MEDIA_LABEL'), {'fields': [
                'thumbnail',
                'thumbnail_text',
                'featured_image',
                'featured_image_text',
            ]}),
        (_('RELATED_LABEL'), {'fields': [
               # 'autor',
                'categories',
                'album',
                'tags',
                'related_posts',
            ]}),
        (_('BASIC_SEO'), {'fields': [
                'slug',
                'meta_title',
                'meta_description',
            ]}),
    ]
    form = PostItemAdminForm
    prepopulated_fields = {"slug": ("title",),}

class PostArticleAdmin(PostItemAdmin):
    form = PostArticleAdminForm
    class Meta:
        verbose_name = _('POST_ARTICLE_TITLE')
        verbose_name_plural = _('POST_ARTICLE_TITLE_PLURAL')
    
    def get_queryset(self, request):
        return self.model.objects.filter(post_type='post')
    
    def save_model(self, request, obj, form, change):
        # custom stuff here
        if not change:
            obj.post_type = "post"  
            if obj.autor is None:
               obj.autor =  request.user
        obj.save()

class PostPageAdmin(PostItemAdmin):
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
                'title',
                'content',
            ]}),
        (_('PUBLISHING_LABEL'), {'fields': [
                'publish',
                'publish_date',
                'is_on_feed',
            ]}),
        (_('MEDIA_LABEL'), {'fields': [
                'thumbnail',
                'thumbnail_text',
                'featured_image',
                'featured_image_text',
            ]}),
        (_('RELATED_LABEL'), {'fields': [
                'autor',
                'categories',
                'album',
                'tags',
                'related_posts',
            ]}),
        (_('BASIC_SEO'), {'fields': [
                'slug',
                'meta_title',
                'meta_description',
            ]}),
    ]   
    form = PostPageAdminForm
    class Meta:
        verbose_name = _('POST_PAGE_TITLE')
        verbose_name_plural = _('POST_PAGE_TITLE_PLURAL')   
    
    def get_queryset(self, request):
        return self.model.objects.filter(post_type='page')
    
    def save_model(self, request, obj, form, change):
        # custom stuff here
        if not change:
            obj.post_type = "page"
            if obj.autor is None:
               obj.autor = request.user

        obj.save()
        
class PostVideoAdmin(PostItemAdmin):
    form = PostVideoAdminForm
    class Meta:
        verbose_name = _('POST_VIDEO_TITLE')
        verbose_name_plural = _('POST_VIDEO_TITLE_PLURAL')   

    def get_queryset(self, request):
        return self.model.objects.filter(post_type='video')

    def save_model(self, request, obj, form, change):
        # custom stuff here
        if not change:
            obj.post_type = "video"
            if obj.autor is None:
               obj.autor = request.user
        obj.save()


create_modeladmin(PostCategoryArticleAdmin, name='category-article', model=PostCategory) 
create_modeladmin(PostCategoryPageAdmin, name='category-page', model=PostCategory) 
create_modeladmin(PostCategoryVideoAdmin, name='category-video', model=PostCategory) 
create_modeladmin(PostPageAdmin, name='post-page', model=PostItem) 
create_modeladmin(PostArticleAdmin, name='post-article', model=PostItem) 
create_modeladmin(PostVideoAdmin, name='post-video', model=PostItem)