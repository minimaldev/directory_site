from django.conf.urls import patterns
from django.conf.urls import url
from media import views 
# Create your views here.
urlpatterns = patterns('',

    url(r'oembed/html/{0,1}$',
        views.oembed_html,     
        name='media_oembed_html'
    ),
    
    url(r'image/upload/{0,1}$',
        views.add_media_image,     
        name='media_add_media_image'
    ),

    url(r'image/featured/{0,1}$',
        views.set_featured_image,     
        name='media_set_featured_image'
    ),

    url(r'image/description/{0,1}$',
        views.set_description_image,     
        name='media_set_description_image'
    ),

    url(r'image/description/(?P<pk>\d+)/{0,1}$',
        views.get_description_image,     
        name='media_get_description_image'
    ),

    url(r'image/remove/{0,1}$',
        views.remove_old_file,     
        name='media_remove_old_file'
    ),

    url(r'video/create/{0,1}$',
        views.add_video_link,     
        name='media_add_video_link'
    ),

    url(r'video/featured/{0,1}$',
        views.set_featured_video,     
        name='media_set_featured_video'
    ),

    url(r'video/description/{0,1}$',
        views.set_description_video,     
        name='media_set_description_video'
    ),

    url(r'video/description/(?P<pk>\d+)/{0,1}$',
        views.get_description_video,     
        name='media_get_description_video'
    ),
    
    url(r'video/remove/{0,1}$',
        views.delete_video_link,     
        name='media_delete_video_link'
    ),

)