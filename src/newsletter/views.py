import json
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import timezone
from django.template.response import TemplateResponse
from django.http import Http404, HttpResponseRedirect, HttpResponse
from newsletter.models import EmailNewsletter
from newsletter.forms  import EmailNewsletterCreationModelForm
from newsletter.forms  import EmailNewsletterUnsuscribeModelForm

def suscribe(request):
    template_name = 'frontend/modules/newsletter/suscribed_successfully.html'
    data = {}
    context = {}
    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = EmailNewsletterCreationModelForm(        
            data=x
        )
        if context['form'].is_valid():
            context['form'].save()
            data['success'] = True

    if request.is_ajax():
        return HttpResponse(
            json.dumps(data), 
            content_type="application/json"
        )        
    else:
        return TemplateResponse(
            request,
            template_name,
            context
        )


def unsuscribe(request):
    template_name = 'frontend/modules/newsletter/unsuscribed_successfully.html'
    data = {}
    context = {}
    data['success'] = False
    
    if request.method == "POST":
        x = request.POST
        context['form'] = EmailNewsletterUnsuscribeModelForm(        
            data=x
        )
        if context['form'].is_valid():
            context['form'].save() 
            data['success'] = True
    
    if request.is_ajax():
        return HttpResponse(
            json.dumps(data), 
            content_type="application/json"
           )        
    else:
        return TemplateResponse(
            request,
            template_name,
            context
        )
