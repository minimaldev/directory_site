#encoding:iso-8859-1
from django import forms
from rss_youtube_videos.models import VideoChannel
from mediamanager.widgets import MediaItemWidget

class VideoChannelAdminForm(forms.ModelForm):
    class Meta:
        model = VideoChannel

    def __init__(self, *args, **kwargs):
        super(VideoChannelAdminForm, self).__init__(*args, **kwargs)
        self.fields['related_channels'].queryset = VideoChannel.objects.exclude(
                id__exact=self.instance.id
            )
