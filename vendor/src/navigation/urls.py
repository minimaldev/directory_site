from django.conf.urls import patterns
from django.conf.urls import url
from navigation import views 
# Create your views here.
urlpatterns = patterns('',

    url(
       r'^navigation/item/get/$',
       'navigation.views.get_item', 
        name="navigation_get_item"
    )

)