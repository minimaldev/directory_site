# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.utils.translation import ugettext_lazy as _
from navigation.models import NavigationPosition
from navigation.models import NavigationItem
from navigation.forms import NavigationItemAdminItemForm, EditNavigationItemAdminItemForm
#from genericadmin.admin import GenericAdminModelAdmin
from django.conf import settings
from django_mptt_admin.admin import DjangoMpttAdmin

class NavigationPositionAdmin(admin.ModelAdmin):

    list_display = ('name', 'created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
            'name',
        ]}),
    ]

class NavigationItemAdmin(DjangoMpttAdmin):
    class Media:
        js = (
               settings.STATIC_URL + '/admin/js/copytitle.js',
               settings.STATIC_URL + '/admin/js/get_content_id_list.js',
            )
    list_display = ('title', 'slug', 'created', 'modified','id',)
    fieldsets = (
        (_('BASIC_LABEL'), {'fields': [
            'title',
        ]}),
        (_('RELATED_LABEL'), {'fields': [
            'parent', 
            'position',          
            'content_type',
            'object_id',
          
        ]}),
        (_('BASIC_SEO'), {'fields': [
            'slug',
            'meta_title',
            'meta_description',
        ]}),     
    )
    prepopulated_fields = {"slug": ("title",),}
    form = NavigationItemAdminItemForm
#    add_fieldsets = (
#        (_('BASIC_LABEL'), {'fields': [
#            'title',
#        ]}),
#        (_('RELATED_LABEL'), {'fields': [
#            'parent',
#            'position',
#            'content_type',
#            'object_id',
#        ]}),
#        (_('BASIC_SEO'), {'fields': [
#            'slug',
#            'meta_title',
#            'meta_description',
#        ]}),     
#    )
#   
#    form = EditNavigationItemAdminItemForm
#    change_form = EditNavigationItemAdminItemForm



admin.site.register(NavigationPosition, NavigationPositionAdmin)
admin.site.register(NavigationItem, NavigationItemAdmin) 

