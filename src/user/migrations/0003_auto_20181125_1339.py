# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_customuser_nick'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='city',
            field=models.CharField(unique=True, max_length=255, verbose_name='CITY_LABEL', blank=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='country',
            field=django_countries.fields.CountryField(default=b'US', max_length=2),
        ),
        migrations.AddField(
            model_name='customuser',
            name='state',
            field=models.CharField(unique=True, max_length=255, verbose_name='STATE_LABEL', blank=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='verified',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='customuser',
            name='zip_code',
            field=models.CharField(unique=True, max_length=255, verbose_name='ZIP_CODE_LABEL', blank=True),
        ),
    ]
