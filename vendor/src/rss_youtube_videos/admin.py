#encoding:iso-8859-1
from mptt.admin import MPTTModelAdmin
from django.utils.translation import ugettext_lazy as _
from rss_youtube_videos.models import VideoChannel
from rss_youtube_videos.models import VideoChannelVisits
from rss_youtube_videos.models import VideoItem
from rss_youtube_videos.forms import VideoChannelAdminForm
from django.conf import settings
from django.contrib import admin
import urllib
import httplib
import json
from urlparse import urlparse
from random import shuffle

class VideoChannelVisitsInline(admin.StackedInline):
    model = VideoChannelVisits
    extra = 0
    readonly_fields = ['customer_email', 'order_id', 'order_item_id']
    exclude = ['created', 'modified','purchased_date','customer_email']


class VideoChannelAdmin(admin.ModelAdmin):
    fieldsets = [
     (_('Basic'),{'fields': [
        'las_api_key_used',
        'have_username',
        'upload_playlist_id',
        'channel_username',
        'visit_per_week',
        'visit_hour',
        'visit_counter',
        'last_visit_date',
        'next_visit_date',
        ]}),
     (_('Related'),{'fields': ['related_channels']}),
    ]

    list_display = ('channel_username', 'created', 'modified')
    search_fields = ['channel_username']
    form = VideoChannelAdminForm
    readonly_fields = ('visit_counter','last_visit_date','next_visit_date','upload_playlist_id','las_api_key_used')
    def save_model(self, request, obj, form, change):
        if (getattr(obj, 'upload_playlist_id', None) is None or getattr(obj, 'channel_username', None) is not None):
            shuffle(settings.GOOGLE_API_KEY)
            for api_key in settings.GOOGLE_API_KEY:
                "using youtuve api v3"
                channel_params = {
                    'part': 'contentDetails',
                    'key' : api_key                    
                }
                if obj.have_username == False:
                    channel_params['id'] = obj.channel_username
                else:
                    channel_params['forUsername'] = obj.channel_username

                obj.las_api_key_used = api_key

                channe_url = 'https://www.googleapis.com/youtube/v3/channels'
                user_data = self.get_youtube_items (channe_url, channel_params)
                uploads_playlist_id = user_data['items'][0]['contentDetails']['relatedPlaylists']['uploads']
                obj.upload_playlist_id = uploads_playlist_id

        obj.save()
    def get_youtube_items (self, url, parameters):
        query = urllib.urlencode(parameters,True)
        URL = urlparse(url)
        connection = httplib.HTTPSConnection(URL.hostname)
        connection.request('GET', URL.path + '?' + query)
        response = connection.getresponse()
        return json.loads(response.read())

class VideoItemAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/js/vendor/tiny_mce/tiny_mce.js',
            '/static/js/textareas.js',
            '/static/js/mediamanager/mediamanager_field.js',
            '/static/js/copytitle.js',
        )
    fieldsets = [
     (_('Basic'),{'fields': [
        'link', 'type_video',
        'title', 'excerpt', 'content',
        'publish', 'publish_date', 'is_in_feed', 'thumb',
        'thumb_text','featured_image','featured_image_text',
      
        ]}),
     (_('Autor'),{'fields': ['user']}),
     (_('Categories'),{'fields': ['categories']}),
     (_('Featured'),{'fields': ['is_featured', 'featured_start_date',
            'featured_end_date']
            }),
     (_('Related Notices'),{'fields': ['related_videos']}),
     (_('Seo'),{'fields': ['slug', 'metatitle', 'metadescription']})
    ]
    list_display = ('title', 'publish', 'is_in_feed', 'is_featured', 
        'user', 'created', 'modified'
        )
    
    search_fields = ['title']
    prepopulated_fields = {'slug':('title',)}
    
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'user', None) is None:
            obj.user = request.user
        obj.save()

admin.site.register(VideoChannel, VideoChannelAdmin)
#admin.site.register(VideoItem, VideoItemAdmin)