from rss_youtube_videos.models import VideoItem
from django.shortcuts import render_to_response
from django.template import RequestContext


def rss_list(request, theme='default'):
 
	template_name = '%s/modules/rss_youtube_videos/rss.html' % theme

    context = {
        'videos': VideoItem.objects.all().order_by('-id'),
    }
    return render_to_response(
            template_name,
            mimetype="text/xml",
            context_instance = RequestContext(request, context)
        )
