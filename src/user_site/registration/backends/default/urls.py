from django.conf.urls import patterns
from django.conf.urls import include
from django.conf.urls import url
from django.views.generic.base import TemplateView

from user_site.registration.backends.default.views import ActivationView
from user_site.registration.backends.default.views import RegistrationView


urlpatterns = patterns('',
        url(r'^activate/complete/$',
            TemplateView.as_view(template_name='frontend/modules/user_site/registration/activation_complete.html'),
            name='user_site_registration_activation_complete'),

        url(r'^activate/(?P<activation_key>\w+)/$',
            ActivationView.as_view(),
            name='user_site_registration_activate'),

        url(r'^register/$',
            RegistrationView.as_view(),
            name='user_site_registration_register'),

        url(r'^register/complete/$',
            TemplateView.as_view(template_name='frontend/modules/user_site/registration/registration_complete.html'),
            name='user_site_registration_complete'
            ),

        url(r'^register/closed/$',
            TemplateView.as_view(template_name='frontend/modules/user_site/registration/registration_closed.html'),
            name='user_site_registration_disallowed'),
        
        url(r'', include('user_site.registration.auth_urls')),
)
