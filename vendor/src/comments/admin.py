# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from comments.models import Comments

class CommentsAdmin(admin.ModelAdmin):

    list_display = ('autor', 'ip', 'created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
            'autor',
            'parent',
            'comment',
            'status',
            'ip',
     
        ]}),
 
    ]
    readonly_fields = ('ip',)

admin.site.register(Comments, CommentsAdmin)