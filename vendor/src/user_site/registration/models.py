import re
import random
import hashlib
import datetime
from django.conf import settings
from django.db import models
from django.db import transaction
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.utils.translation import ugettext_lazy as _

from user_site.models import UserSite as User

try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now

SHA1_RE = re.compile('^[a-f0-9]{40}$')

class RegistrationManager(models.Manager):
    def activate_user(self, activation_key): 
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return False
            if not profile.activation_key_expired():
                user = profile.user_site
                user.is_active = True
                user.save()
                profile.activation_key = self.model.ACTIVATED
                profile.save()
                return user
        return False
    
    def create_inactive_user(self,  email, password,  send_email=True):

        new_user = User.objects.create_user( email, password)
        
        new_user.is_active = False
        new_user.save()
        registration_profile = self.create_profile(new_user)
        if send_email:
            registration_profile.send_activation_email()
        return new_user

    create_inactive_user = transaction.atomic(create_inactive_user)

    def create_profile(self, user):
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = user.email
        if isinstance(username, unicode):
            username = username.encode('utf-8')
        activation_key = hashlib.sha1(salt+username).hexdigest()
        return self.create(
            user_site=user,
            activation_key=activation_key
        )
        
    def delete_expired_users(self):
        for profile in self.all():
            try:
                if profile.activation_key_expired():
                    user = profile.user_site
                    if not user.is_active:
                        user.delete()
                        profile.delete()
            except User.DoesNotExist:
                profile.delete()

class RegistrationProfile(models.Model):

    ACTIVATED = u"ALREADY_ACTIVATED"    
    user_site = models.ForeignKey(User,  verbose_name=_('UserSite'))    
    activation_key = models.CharField(_('activation key'), max_length=40)    
    objects = RegistrationManager()    
    activation_key_expired = True

    class Meta:
        verbose_name = _('registration profile')
        verbose_name_plural = _('registration profiles')
    
    def __unicode__(self):
        return u"Registration information for %s" % self.user_site
    
    def activation_key_expired(self):
        expiration_date = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.activation_key == self.ACTIVATED or \
               (self.user_site.created + expiration_date <= datetime_now())
    
    def send_email(self, template, subject, from_email, to_email=[],context = {}):
        d = Context(context)
        plaintext = get_template('modules/user_site/registration/%s.txt' % template)
        htmly = get_template('modules/user_site/registration/%s.html' % template)    
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    
    def send_activation_email(self):
        ctx_dict = {
                'activation_key': self.activation_key,
                'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                #'site': site
            }
        #subject = render_to_string(
        #        'auth/registration/activation_email_subject.html',
        #        ctx_dict
        #    )
        #subject = ''.join(subject.splitlines())        
        #message = render_to_string(
        #        'auth/registration/activation_email.html',
        #        ctx_dict
        #    )
        # 
        subject = "Activate Your account"
        self.send_email(
                template='activation_email',
                subject=subject,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to_email=[self.user_site.email],
                context=ctx_dict
            )  
        #self.user_site.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)
    
