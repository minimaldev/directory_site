from django.db.models import Q
from django.template.response import TemplateResponse
from django.utils import timezone
from utilities.paginator import paginator
from posts.models import PostCategory
from posts.models import PostItem
from globaly.models import GlobalyTags
from posts.forms import SearchForm
from django.shortcuts import get_object_or_404

def category(request, slug=None, page=1, model=None):

    context = {}
    current_date =  timezone.now()

    f1 = Q(publish=True)
    f6 = Q(slug=slug)

    if model is None:
        model = get_object_or_404(PostCategory, f1 & f6 )
    
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(categories__id=model.id)    
    f5 = Q(post_type="post")    

    context['category'] = model
    context['slug'] = slug    
    context['posts'] = paginator(
                page, 
                PostItem.objects.filter(
                    f1 & (f2 | f3) & f4 & f5
                ).order_by("-publish_date",'-id'),
                
            )

    template_name = [
            'frontend/modules/%s/category-%d.html' % (model.post_type, model.id),
            'frontend/modules/%s/category-%s.html' % (model.post_type, slug),
            'frontend/modules/%s/category.html' % model.post_type
        ]
    return TemplateResponse(request, template_name, context)

def post_details(request, slug=None, model=None):
    
    context = {}
    current_date =  timezone.now()
    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None) 
    f4 = Q(slug=slug)

    if model is None:
        model = get_object_or_404(PostItem, f1 & (f2 | f3) & f4  )
    
    context['post'] = model
    context['related_posts'] = context['post'].related_posts.filter( 
            f1 & (f2 | f3) 
        ).order_by("-publish_date",'-id')

    context['tags'] = context['post'].tags.all()
    
    template_name =  [
            'frontend/modules/%s/post-%d.html' % (model.post_type, model.id),
            'frontend/modules/%s/post-%s.html' % (model.post_type, slug),
            'frontend/modules/%s/post.html' % model.post_type,
            
        ] 
    return TemplateResponse(request, template_name, context)

def autor(request, slug=None, page=1, model=None):

    context = {}
    current_date =  timezone.now()

    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(autor__nick=slug)    
    f5 = Q(post_type="post")

    post_type = "post"

    context['autor'] = slug

    context['posts'] = paginator(
                page, 
                PostItem.objects.filter(
                    f1 & (f2 | f3) & f4 & f5 
                ).order_by("-publish_date",'-id')
                
            )

    template_name = [
            #'frontend/modules/%s/autor-%d.html' % (post_type, model.id),
            'frontend/modules/%s/autor-%s.html' % (post_type, context['autor']),
            'frontend/modules/%s/autor.html' % post_type
        ]
    return TemplateResponse(request, template_name, context)

def tags(request, slug=None, page=1, model=None):

    context = {}
    current_date =  timezone.now()

    f1 = Q(publish=True)
    f2 = Q(publish_date__lte=current_date)
    f3 = Q(publish_date=None)
    f4 = Q(tags__slug=slug)    
    f5 = Q(post_type="post")  

    post_type = "post"
    
    context['tag'] = get_object_or_404( GlobalyTags,slug=slug)
    context['posts'] = paginator(
                page, 
                PostItem.objects.filter(
                    f1 & (f2 | f3) & f4 & f5 
                ).order_by("-publish_date",'-id')
                
            )

    template_name = [
            #'frontend/modules/%s/autor-%d.html' % (post_type, model.id),
            'frontend/modules/%s/tag-%s.html' % (post_type, context['tag']),
            'frontend/modules/%s/tag.html' % post_type
        ]
    return TemplateResponse(request, template_name, context)
