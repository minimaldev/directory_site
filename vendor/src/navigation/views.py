from django.shortcuts import render
from django.db.models import Q
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
import json
from django.contrib.contenttypes.models import ContentType

@staff_member_required
def get_item(request):
    data = {'items': []}
    id = request.GET.get("id", None)
    if id is not None and len(id) > 0:
    	obj = ContentType.objects.get_for_id(id)
    	data['items'] = [ {'id': x.id, "name": x.title_name()} for  x in obj.model_class().get_items_navigation()]
    return  HttpResponse(json.dumps(data), content_type="application/json")