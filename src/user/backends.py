from user.models import CustomUser as UserModel
class CustomUserBackend(object):

    def authenticate(self, email=None, password=None): 

        if email is None:
            email = kwargs.get(UserModel.USERNAME_FIELD)

        try:
            user =  UserModel.objects.get(email=email)
            
            if user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            UserModel().set_password(password)

    def get_user(self, user_id):
      
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None