import os 
import sys
from django import forms
from datetime import timedelta
from django.utils import timezone
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from utilities.forms import MultiFileField,MultiCharField
import json
import redis
import base64
import hashlib
import itertools
from django.db.models import Max
from utilities.media import get_oembed_html
from media.models import MediaAlbum
from media.models import MediaImage
from media.models import MediaVideos
from directory.models import DirectoryItem

r = redis.StrictRedis(host='localhost', port=6379, db=0)
#crear fomulario de agregar/editar comentario
class MessagesUploadForm(forms.Form):
    files = MultiFileField(
            max_num=10, 
            min_num=1, 
            maximum_file_size=1024*1024*18,
            required=False
        )
    GUID = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 

    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        super(MessagesUploadForm, self).__init__(*args, **kwargs)

    def handle_uploaded_file(self, f, file_path):
        path = default_storage.save(file_path, ContentFile(f.read()))
        return path

    def save(self):
        file_names = self.files.getlist("files")
        GUID = self.cleaned_data['GUID']
        images = []
        images2 = []
        file_path = os.path.join(settings.MEDIA_ROOT, 'uploads')
        directory_path = os.path.join(file_path, str(int(self._user_site.id)))
        if not os.path.exists(directory_path):
             os.mkdir(directory_path)

        for file_name in file_names:
            filename, file_extension = os.path.splitext(file_name.name)
            m = hashlib.sha224(filename).hexdigest()

            filename = "%s%s" %(m,file_extension)
            file_path = os.path.join(directory_path, filename)
            if not os.path.isfile(file_path):
                self.handle_uploaded_file(file_name, file_path)
            image = {}
            image['path'] = file_path
            image['name'] = filename
            image['url'] = 'uploads/%s/%s' %(str(int(self._user_site.id)) , filename)

            I = MediaImage()
            I.name = image.get('name')
            I.image = image.get('url')
            I.content = ""
            I.save()
            image['id'] = int(I.id) 
            images.append(image) 
            images2.append({
                    'name': filename,
                    'id': int(I.id) ,
                    'url': '%s/uploads/%s/%s' %(
                        settings.MEDIA_URL, 
                        str(int(self._user_site.id)),
                        filename
                    )
                })
            
        json_images = json.dumps(images)

        jsons = r.get('images_%s' % (GUID))
        
        if jsons is not None:
            unpacked_images = json.loads(jsons)
            if len(unpacked_images) > 0:
                json_images = json.dumps(
                    list(itertools.chain(
                        images, 
                        unpacked_images
                    )
                ))    
                r.set('images_%s' % (GUID) , json_images)
            else:
                r.set('images_%s' % (GUID) , json_images)
        else:
            r.set('images_%s' % (GUID) , json_images)

        return images2
           
def remove_old_file(filename, pk):
    #save the the new filename
    directory_path = "%s%s" % (settings.MEDIA_ROOT, 'uploads/')
    source = os.path.join(
            directory_path, 
            ('%s/%s' %(pk, filename.name))
        )
    if os.path.isfile(source):
        os.remove(source)


class ImageDeleteForm(forms.Form):
    pk = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=False,
        widget=forms.HiddenInput()
    ) 
    
    
    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        super(ImageDeleteForm, self).__init__(*args, **kwargs)

    def filter_image(
            self, 
            pk,
            unpacked_images
        ):
        selected_image = filter(lambda p: \
                pk == p['id'],
                unpacked_images
            )
        return next(iter(selected_image), None)
    
    def delete_image(self, pk):
        MediaImage.objects.filter(
            pk=pk
        ).delete()

    def delete_image_album(
            self, 
            pk,
            image
        ):
        album_ids = []
        for album in image.album.select_related():
            album_ids.append(album.id)

        if len(album_ids) > 0:
            directory = DirectoryItem.objects.filter(
                album__id__in=album_ids
            )
             
            if len(directory) > 0:
                self.delete_image(pk)

    def delete(self):
        
        GUID = self.cleaned_data['GUID']
        pk = int(self.cleaned_data['pk'])
        
        image = next(iter(MediaImage.objects.filter(
                    pk=pk
                )),None)
        if image is not None:
            jsons = r.get('images_%s' % (GUID))        
            if jsons is not None:
                unpacked_images = json.loads(jsons)
                image = self.filter_image(pk, unpacked_images)  
                if image is not None:
                    self.delete_image(pk)
                else:
                    self.delete_image_album(pk,image)
            else:
                self.delete_image_album(pk,image)
                

class ImageFeaturedForm(forms.Form):
    pk = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    
    featured = forms.CharField(
        max_length=1, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        
        super(ImageFeaturedForm, self).__init__(*args, **kwargs)

    def filter_image(
            self, 
            pk,
            unpacked_images
        ):
        selected_image = filter(lambda p: \
                pk == p['id'],
                unpacked_images
            )
        return next(iter(selected_image), None)
    
    def set_featured_image(self, pk,f):
        MediaImage.objects.filter(
            pk=pk
        ).update(featured=f)

    def set_featured_image_album(
            self, 
            pk,
            image,
            f
        ):
        album_ids = []
        for album in image.album.select_related():
            album_ids.append(album.id)

        if len(album_ids) > 0:
            directory = DirectoryItem.objects.filter(
                album__id__in=album_ids
            )
             
            if len(directory) > 0:
                self.set_featured_image(pk, f)

    def set_featured(self):
        
        GUID = self.cleaned_data['GUID']
        pk = int(self.cleaned_data['pk'])
        f = int(self.cleaned_data.get('featured','0')) == 1
        
        image = next(iter(MediaImage.objects.filter(
                    pk=pk
                )),None)
        if image is not None:
            jsons = r.get('images_%s' % (GUID))        
            if jsons is not None:
                unpacked_images = json.loads(jsons)
                image = self.filter_image(pk, unpacked_images)  
                if image is not None:
                    self.set_featured_image(pk, f)
                else:
                    self.set_featured_image_album(pk, image, f)
            else:
                self.set_featured_image_album(pk, image, f)
                

class ImageDescriptionForm(forms.Form):
    pk = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    
    description = forms.CharField(       
        required=True,
        widget=forms.Textarea()
    ) 
    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        
        super(ImageDescriptionForm, self).__init__(*args, **kwargs)

    def filter_image(
            self, 
            pk,
            unpacked_images
        ):
        selected_image = filter(lambda p: \
                pk == p['id'],
                unpacked_images
            )
        return next(iter(selected_image), None)
    
    def set_description_image(self, pk,description):
        MediaImage.objects.filter(
            pk=pk
        ).update(content=description)

    def set_description_image_album(
            self, 
            pk,
            image,
            description
        ):
        album_ids = []
        for album in image.album.select_related():
            album_ids.append(album.id)

        if len(album_ids) > 0:
            directory = DirectoryItem.objects.filter(
                album__id__in=album_ids
            )
             
            if len(directory) > 0:
                self.set_description_image(pk, description)

    def set_description(self):
        
        GUID = self.cleaned_data['GUID']
        pk = int(self.cleaned_data['pk'])
        description = self.cleaned_data.get('description')
        
        image = next(iter(MediaImage.objects.filter(
                    pk=pk
                )),None)
        if image is not None:
            jsons = r.get('images_%s' % (GUID))        
            if jsons is not None:
                unpacked_images = json.loads(jsons)
                image = self.filter_image(pk, unpacked_images)  
                if image is not None:
                    self.set_description_image(pk, description)
                else:
                    self.set_description_image_album(pk, image, description)
            else:
                self.set_description_image_album(pk, image, description)


class VideoForm(forms.Form):
    link = forms.CharField(
        max_length=1850, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    def __init__(self, *args, **kwargs):
        super(VideoForm, self).__init__(*args, **kwargs)

    def save(self):
        GUID = self.cleaned_data['GUID']
        link = self.cleaned_data['link']
       
        videos = []
        V = MediaVideos()
        V.link = link
        V.content = ""
        V.save()
        videos.append({               
                'id': int(V.id),
                'html': get_oembed_html(link).get('html')
            })
        json_videos = json.dumps(videos)
        jsons = r.get('videos_%s' % (GUID))
        if jsons is not None:
            unpacked_videos = json.loads(jsons)
            if len(unpacked_videos) > 0:
                json_videos = json.dumps(
                    list(itertools.chain(
                        videos, 
                        unpacked_videos
                    )
                ))    
                r.set('videos_%s' % (GUID) , json_videos)
            else:
                r.set('videos_%s' % (GUID) , json_videos)
        else:
            r.set('videos_%s' % (GUID) , json_videos)
        return videos

class VideoDeleteForm(forms.Form):
    pk = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=False,
        widget=forms.HiddenInput()
    ) 
    
    
    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        super(VideoDeleteForm, self).__init__(*args, **kwargs)

    def filter_video(
            self, 
            pk,
            unpacked_videos
        ):
        selected_video = filter(lambda p: \
                pk == p['id'],
                unpacked_videos
            )
        return next(iter(selected_video), None)
    
    def delete_video(self, pk):
        MediaVideos.objects.filter(
            pk=pk
        ).delete()

    def delete_video_album(
            self, 
            pk,
            video
        ):
        album_ids = []
        for album in video.album.select_related():
            album_ids.append(album.id)

        if len(album_ids) > 0:
            directory = DirectoryItem.objects.filter(
                album__id__in=album_ids
            )
             
            if len(directory) > 0:
                self.delete_video(pk)

    def delete(self):
        
        GUID = self.cleaned_data['GUID']
        pk = int(self.cleaned_data['pk'])
        
        video = next(iter(
            MediaVideos.objects.filter(
                    pk=pk
                )),
            None
        )
        if video is not None:
            jsons = r.get('images_%s' % (GUID))        
            if jsons is not None:
                unpacked_videos = json.loads(jsons)
                video = self.filter_video(pk, unpacked_videos)  
                if video is not None:
                    self.delete_video(pk)
                else:
                    self.delete_video_album(pk, video)
            else:
                self.delete_video_album(pk, video)

class VideoFeaturedForm(forms.Form):
    pk = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    
    featured = forms.CharField(
        max_length=1, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        
        super(VideoFeaturedForm, self).__init__(*args, **kwargs)

    def filter_video(
            self, 
            pk,
            unpacked_videos
        ):
        selected_video = filter(lambda p: \
                pk == p['id'],
                unpacked_videos
            )
        return next(iter(selected_video), None)
    
    def set_featured_video(self, pk,f):
        MediaVideos.objects.filter(
            pk=pk
        ).update(featured=f)

    def set_featured_video_album(
            self, 
            pk,
            video,
            f
        ):
        album_ids = []
        for album in video.album.select_related():
            album_ids.append(album.id)

        if len(album_ids) > 0:
            directory = DirectoryItem.objects.filter(
                album__id__in=album_ids
            )
             
            if len(directory) > 0:
                self.set_featured_video(pk, f)

    def set_featured(self):
        
        GUID = self.cleaned_data['GUID']
        pk = int(self.cleaned_data['pk'])
        f = int(self.cleaned_data.get('featured','0')) == 1
        
        video = next(iter(MediaVideos.objects.filter(
                    pk=pk
                )),None)
        if video is not None:
            jsons = r.get('videos_%s' % (GUID))        
            if jsons is not None:
                unpacked_videos = json.loads(jsons)
                video = self.filter_video(pk, unpacked_videos)  
                if video is not None:
                    self.set_featured_video(pk, f)
                else:
                    self.set_featured_video_album(pk, video, f)
            else:
                self.set_featured_video_album(pk, video, f)


class VideoDescriptionForm(forms.Form):
    pk = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        required=True,
        widget=forms.HiddenInput()
    ) 
    
    description = forms.CharField(       
        required=True,
        widget=forms.Textarea()
    ) 
    def __init__(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self._user_site = kwargs.pop('user_site', None)
        
        super(VideoDescriptionForm, self).__init__(*args, **kwargs)

    def filter_video(
            self, 
            pk,
            unpacked_videos
        ):
        selected_video = filter(lambda p: \
                pk == p['id'],
                unpacked_videos
            )
        return next(iter(selected_video), None)
    
    def set_description_video(self, pk,description):
        MediaVideos.objects.filter(
            pk=pk
        ).update(content=description)

    def set_description_video_album(
            self, 
            pk,
            video,
            description
        ):
        album_ids = []
        for album in video.album.select_related():
            album_ids.append(album.id)

        if len(album_ids) > 0:
            directory = DirectoryItem.objects.filter(
                album__id__in=album_ids
            )
             
            if len(directory) > 0:
                self.set_description_video(pk, description)

    def set_description(self):
        
        GUID = self.cleaned_data['GUID']
        pk = int(self.cleaned_data['pk'])
        description = self.cleaned_data.get('description')
        
        video = next(iter(MediaVideos.objects.filter(
                    pk=pk
                )),None)
        if video is not None:
            jsons = r.get('videos_%s' % (GUID))        
            if jsons is not None:
                unpacked_videos = json.loads(jsons)
                video = self.filter_video(pk, unpacked_videos)  
                if video is not None:
                    self.set_description_video(pk, description)
                else:
                    self.set_description_video_album(pk, video, description)
            else:
                self.set_description_video_album(pk, video, description)