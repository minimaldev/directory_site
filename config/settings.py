import os, sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
# append src folder and fixing some import errors
sys.path.insert(1,os.path.join(BASE_DIR, 'src')) 
from django.conf import settings

SSL = False

if SSL == True:
    SESSION_COOKIE_SECURE = SSL
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
    CSRF_COOKIE_SECURE = SSL
    SECURE_SSL_REDIRECT = SSL

SECRET_KEY = 'nv-=_gl5d(^vd05=9waomq%n_m%=(j98zesf^@$$@erz7-k=j5'

DEBUG = True

ALLOWED_HOSTS = [
    'localhost', 
    '127.0.0.1', 
]

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django_markup',
    'django_countries',
    'captcha',
    'widget_tweaks',
    'mptt',
    'form_utils',
    'custom_admin',
    'user',
    'rss',
    'sitemap',
    'utilities',   
    'media',
    'globaly',
    'global_config',
    'contact_form',  
    'user_site',   
    'user_site.social', 
    'user_site.registration',   
    'social_auth',   
    'directory',  
    'newsletter',  


)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'globaly.middleware.SearchMiddleware',
    'directory.middleware.DirectoryUrlsMiddleware',
    #'posts.middleware.BlogUrlsMiddleware',
    #'navigation.middleware.UrlsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'user_site.middleware.UserSiteAuthMiddleware',
)

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'public/templates/frontend/'),
            os.path.join(BASE_DIR, 'public/templates/backend/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'sitemap.context_processors.get_domain',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'user_site.context_processors.user_site',
                'utilities.context_processors.get_domain',
            ],
        },
    },
]

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)


EXCLUDE_AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

AUTHENTICATION_BACKENDS = ( 
    'src.user_site.backends.UserSiteBackend',
    'src.social_auth.backends.facebook.FacebookBackend',
    'src.social_auth.backends.twitter.TwitterBackend',
    'src.social_auth.backends.google.GoogleOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',    
)


WSGI_APPLICATION = 'config.wsgi.application'

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Caracas'

APPEND_SLASH = True

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', u'English'),
    ('es', u'Spanish'),
)

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'config/locale/')
]

DEFAUT_THEME = 'default'

MEDIA_URL = '/assets/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'public/content/media/')

MEDIA_UPLOAD_ROOT = os.path.join(BASE_DIR, 'public/content/media/uploads/')

STATIC_URL = '/assets/'

PUBLIC_DIR = os.path.join(BASE_DIR, 'public')

if DEBUG == False:
    STATIC_ROOT = os.path.join(BASE_DIR, 'public/static/')
else:
    STATICFILES_DIRS = (
        os.path.join(BASE_DIR, 'public/static/backend'),
        os.path.join(BASE_DIR, 'public/static/dashboard'),
        os.path.join(BASE_DIR, 'public/static/frontend'),
        os.path.join(BASE_DIR, 'public/content/'),
    )

STATIC_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

try:
    from local_settings import *
except ImportError:
    pass

DATABASES = {
    'default': {
         #unitylatino_django
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'webdisenoya_emprendimientos_directorio_web',
        'USER': 'root',
        'PASSWORD': 'demo',
        'CONN_MAX_AGE': 0,
    }
}

#cd /home/unitylatino & source venv/bin/activate & pip install -r requirements.txt
#
AUTH_USER_MODEL = 'user.CustomUser'

RECAPTCHA_PUBLIC_KEY = '6LdB3RMTAAAAAI8KOCSQFrS9PnSim4H-d7mDQq99'
RECAPTCHA_PRIVATE_KEY = '6LdB3RMTAAAAAGzpNezah55TZwtOqCSApJJ_bcDy'
#CAPTCHA_AJAX = True
NOCAPTCHA = True
RECAPTCHA_USE_SSL = True

USER_SITE_LOGIN_URL = "/account/login"

#social auth
TWITTER_CONSUMER_KEY         = 'ttbudzVAFIxuTB57MQP9OH40o'
TWITTER_CONSUMER_SECRET      = 'f7mRnGNEmIwyigHs2pyGoKX61t2bf9m3tP7vjZHTqswU7yH29j'
TWITTER_ACCESS_TOKEN = "2348736770-nLXOXhMVj8i7hHHrLkkOBZUF7WOtaqwkBjxr87x"
TWITTER_ACCESS_TOKEN_SECRET = "aKSLI9w0iN1REAXI5kjdpdbUD5FE2eRIuY0eCEfWnP6Pe"

INSTAGRAM_CONSUMER_KEY       = '0c6c43f9a9a84952a92792bed384f4bc'
INSTAGRAM_CONSUMER_SECRET    = '4bb8aac64adb47a0a219d1d7e2365265'
FACEBOOK_APP_ID              = '1713537162191105'
FACEBOOK_API_SECRET          = 'afc2890286755edb099b83e596f40420'
GOOGLE_OAUTH2_CLIENT_ID      = '748652545139-2d4r2rb2qjs13ognsu1d45qa16ipihk8.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET  = 'tjcs3_QuGP0YnVmT2aGP8BDz'
GOOGLE_OAUTH2_USE_UNIQUE_USER_ID = True

SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True

FACEBOOK_EXTENDED_PERMISSIONS = ['email','public_profile']
SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email',]
#SOCIAL_AUTH_USER_MODEL = 'user_site.UserSite'
SOCIAL_AUTH_REVOKE_TOKENS_ON_DISCONNECT = True

EMAIL_SECONDS_TIMEOUT = 15
EMAIL_EXPIRED_SECONDS_TIMEOUT = 18

PAYPAL_CURRENCY = "USD"
PAYPAL_EMAIL = "yaraujo@outlook.com"
WALLET_PAYPAL_EMAIL = PAYPAL_EMAIL
PAYPAL_PAYMENT_MODE = "live" # live or sandbox