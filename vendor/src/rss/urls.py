from django.conf.urls import patterns
from django.conf.urls import url
from utilities.generic.base import TemplateView
from rss import views 
# Create your views here.
urlpatterns = patterns('',

    url(r'^rss.xml$',
            views.rss,
            name='rss'
        ),
    
)