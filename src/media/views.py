import json
from django.http import Http404, HttpResponseRedirect, HttpResponse
from src.user_site.decorators import login_required
from utilities.media import get_oembed_html
from media.forms import MessagesUploadForm, ImageDeleteForm, ImageFeaturedForm
from media.forms import ImageDescriptionForm
from media.forms import VideoDescriptionForm
from media.forms import VideoForm, VideoDeleteForm, VideoFeaturedForm, VideoFeaturedForm
from media.models import MediaAlbum
from media.models import MediaImage
from media.models import MediaVideos

def oembed_html(request):
	context = {'html':{}}
	link = request.GET.get('link')
	if link:
		html =  get_oembed_html(link).get('html')
		if html is not None:
			context['html'] = html
	return HttpResponse(json.dumps(context), content_type="application/json")

@login_required
def add_media_image(request):
    data = {}
    context = {}

    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = MessagesUploadForm(
            user_site=request.user_site,
            request=request,               
            data=x,
            files=request.FILES
        )
        if context['form'].is_valid():
            images = context['form'].save() 
            data['images'] = images
            data['success'] = True

    # just return a JsonResponse
    return HttpResponse(json.dumps(data), content_type="application/json")

@login_required
def remove_old_file(request):
    data = {}
    context = {}

    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = ImageDeleteForm(
            user_site=request.user_site,
            request=request,               
            data=x
        )
        if context['form'].is_valid():
            data['success'] = True
            data['id'] = int(context['form'].cleaned_data['pk'])
            context['form'].delete()

    return HttpResponse(
        json.dumps(data), 
        content_type="application/json"
    )

@login_required
def set_featured_image(request):
    data = {}
    context = {}

    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = ImageFeaturedForm(
            user_site=request.user_site,
            request=request,               
            data=x
        )        
        if context['form'].is_valid():
            data['success'] = True
            data['id'] = int(context['form'].cleaned_data['pk'])            
            context['form'].set_featured()

    return HttpResponse(json.dumps(data), content_type="application/json")

@login_required
def set_description_image(request):
    data = {}
    context = {}

    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = ImageDescriptionForm(
            user_site=request.user_site,
            request=request,               
            data=x
        )
       
        if context['form'].is_valid():
            data['success'] = True
            data['id'] = int(context['form'].cleaned_data['pk'])            
            context['form'].set_description()

    return HttpResponse(json.dumps(data), content_type="application/json")

@login_required
def get_description_image(request, pk):
    data = {}
    context = {}
    data['success'] = False
    image = next(iter(
        MediaImage.objects.filter(
            pk=pk))
        , 
        None
    )
    if image is not None:
        data['success'] = True
        data['description'] = image.content

    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
def add_video_link(request):
    data = {}
    context = {}
    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = VideoForm(       
            data=x
        )        
        if context['form'].is_valid():
            data['success'] = True
            videos = context['form'].save() 
            data['videos'] = videos

    return HttpResponse(
        json.dumps(data), 
        content_type="application/json"
    )

@login_required
def delete_video_link(request):
    data = {}
    context = {}
    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = VideoDeleteForm(       
            data=x
        )        
        if context['form'].is_valid():
            data['success'] = True
            data['id'] = int(context['form'].cleaned_data['pk'])         
            context['form'].delete()

    return HttpResponse(
        json.dumps(data), 
        content_type="application/json"
    )

@login_required
def set_description_video(request):
    data = {}
    context = {}

    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = VideoDescriptionForm(
            user_site=request.user_site,
            request=request,               
            data=x
        )
        
        if context['form'].is_valid():
            data['success'] = True
            data['id'] = int(context['form'].cleaned_data['pk'])            
            context['form'].set_description()

    return HttpResponse(
        json.dumps(data), 
        content_type="application/json"
    )

@login_required
def set_featured_video(request):
    data = {}
    context = {}

    data['success'] = False
    if request.method == "POST":
        x = request.POST
        context['form'] = VideoFeaturedForm(
            user_site=request.user_site,
            request=request,               
            data=x
        )        
        if context['form'].is_valid():
            data['success'] = True
            data['id'] = int(context['form'].cleaned_data['pk'])
            
            context['form'].set_featured()

    return HttpResponse(
        json.dumps(data), 
        content_type="application/json"
    )

@login_required
def get_description_video(request, pk):
    data = {}
    context = {}
    data['success'] = False
    video = next(iter(
        MediaVideos.objects.filter(
            pk=pk))
        , 
        None
    )
    if video is not None:
        data['success'] = True
        data['description'] = video.content

    return HttpResponse(
        json.dumps(data), 
        content_type="application/json"
    )
