from django.conf.urls import patterns
from django.conf.urls import url
from directory import views 
# Create your views here.
urlpatterns = patterns('',
    
    #url(r'^$',
    #        views.home,
    #        name='blog_home'
    #        ),
    #    
    #url(r'^page/(?P<page>\d+)/{0,1}$',
    #        views.home,
    #        name='blog_home'),
    #
    url(r'^category/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
            views.choicess,
            name='category'
        ),
    url(r'^category/(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
            views.choicess,
            name='category'
        ),

    url(r'^search/{0,1}$',
            views.search,
            name='directory_search'
        ),
    url(r'^search/page/(?P<page>\d+)/{0,1}$',
            views.search,
            name='directory_search'
    ), 
    url(r'^search/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
            views.search,
            name='directory_search'
        ),
    url(r'^search/(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
            views.search,
            name='directory_search'
        ),
    
    #url(r'^(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
    #        views.category,
    #        name='category'
    #    ),
    #
    url(r'^item/details/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
           views.post_details,
           name='directory_post_details'
        ),
    
    url(r'^autor/(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
        views.autor,
        name='directory_autor'
    ),

    url(r'^autor/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
           views.autor,
           name='directory_autor'
    ),
    
    url(r'^tag/(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
        views.tags,
        name='directory_tags'
    ),

    url(r'^tag/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
           views.tags,
           name='directory_tags'
    ),
    url(r'^profile/(?P<username>\w+)/{0,1}$',
            views.public_profile,
            name='directory_public_profile'
    ), 

    url(r'^profile/(?P<username>\w+)/(?P<page>\d+)/{0,1}$',
            views.public_profile,
            name='directory_public_profile'
    ),         
)