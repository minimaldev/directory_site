from django.conf.urls import patterns
from django.conf.urls import url
from posts import views 
# Create your views here.
urlpatterns = patterns('',
    
    #url(r'^$',
    #        views.home,
    #        name='blog_home'
    #        ),
    #    
    #url(r'^page/(?P<page>\d+)/{0,1}$',
    #        views.home,
    #        name='blog_home'),
    #
    url(r'^category/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
            views.category,
            name='category'
        ),

    #url(r'^(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
    #        views.category,
    #        name='category'
    #    ),
    #
    #url(r'^(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
    #       views.post_details,
    #       name='post_details'
    #    ),
    
    url(r'^autor/(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
        views.autor,
        name='posts_autor'
    ),

    url(r'^autor/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
           views.autor,
           name='posts_autor'
    ),
    
    url(r'^tag/(?P<slug>[0-9A-Za-z-_]+)/page/(?P<page>\d+)/{0,1}$',
        views.tags,
        name='posts_tags'
    ),

    url(r'^tag/(?P<slug>[0-9A-Za-z-_]+)/{0,1}$',
           views.tags,
           name='posts_tags'
    ),

)