import random
from django.conf import settings

def randon_index(request):
    return {
        'randon_text': random.randint(0,1),
    }

def get_domain(request):
    if request.is_secure():
        protocol = 'https'
    else:
        protocol = 'http'
    return {
        'current_domain': protocol+"://"+request.get_host() ,
    }

