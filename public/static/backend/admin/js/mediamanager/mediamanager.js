(function($)
{
	if(window.opener)
	{
	
		$(window).load(function()
		{
			var isPopup = function()
			{
			    return window.location.href.search("[?&]_popup=1") != -1;
			};

			if (isPopup()==true)
			{
				$("img").css('cursor','pointer');
				//$(".field-title,.field-rating").css('display','none');
			}

			
			function cancelpress(event)
			{
				event.preventDefault();
				var selector_id = "#id_"+window.opener.selector_id
				var elem = window.opener.django.jQuery(selector_id);
				var image_url = $(this).attr('data-src');
				var alt_text = $(this).attr('data-title');


				    if (elem.is("input")) 
				    {
				    	elem.val(image_url);
				    	window.opener.django.jQuery(selector_id+"_text").val(alt_text);
				    }
				    else if (elem.is("textarea")) 
				    {
				    	r = '<img alt="'+alt_text+'" src="'+image_url+'" />\n'
				        //window.opener.django.jQuery(selector_id).insertAtCaret();			               
				    	window.opener.myEditor.codemirror.replaceSelection(r)
				    	//window.opener.django.jQuery(selector_id).trigger('change');
				    }    	
				window.close();
			}
			$(".field-thumb img").on('click', cancelpress)
		});	
	 
		
	}

})(django.jQuery);