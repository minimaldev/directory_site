# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin import RelatedFieldListFilter, RelatedOnlyFieldListFilter
from django.utils.translation import ugettext_lazy as _
from directory.models import DirectoryCategory
from directory.models import DirectoryItem
from directory.forms import DirectoryItemAdminForm
from directory.forms import DirectoryCategoryAdminForm
from utilities.admin import create_modeladmin
from django.conf import settings

class DirectoryCategoryAdmin(admin.ModelAdmin):
    class Media:
        js = (settings.STATIC_URL + 'admin/js/copytitle.js',)
    list_display = ('name', 'created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
            'parent',
            'name',
            'publish'
        ]}),
        (_('BASIC_SEO'), {'fields': [
            'slug',
            'meta_title',
            'meta_description',
        ]}),     
    ]
    prepopulated_fields = {"slug": ("name",),}

class DirectoryItemAdmin(admin.ModelAdmin):
    class Media:
        js = (
            #'//cdn.jsdelivr.net/simplemde/latest/simplemde.min.js',
            settings.STATIC_URL + 'js/vendor/simplemde.min.js',
            settings.STATIC_URL + 'js/textareas.js',
            settings.STATIC_URL + 'admin/js/mediamanager/mediamanager_field.js',
            settings.STATIC_URL + 'admin/js/copytitle.js',
            settings.STATIC_URL + 'admin/js/album/album_field.js',
        )
        css = {
             'all': (
                '//cdn.jsdelivr.net/simplemde/latest/simplemde.min.css',
                'https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
                #settings.STATIC_URL + 'frontend/css/simplemde.css',
                )
        }  
    list_filter = (
        ('autor', RelatedOnlyFieldListFilter),
        ('categories', RelatedOnlyFieldListFilter),
        'publish',
        # ...
    )        
    list_display = [
            'title', 'thumb', 'Tags', 
            'Categories','autor' , 'created', 'modified',
            'id',
        ]
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [
                'title',
                'content',
                'excerpt',
            ]}),
        (_('PUBLISHING_LABEL'), {'fields': [
                'publish',
                'publish_date',
                'release_date',
                'is_featured',
                'featured_start_date',
                'featured_end_date',
                'is_on_feed',
            ]}),
        (_('MEDIA_LABEL'), {'fields': [
                'thumbnail',
                'thumbnail_text',
                'featured_image',
                'featured_image_text',
            ]}),
        (_('RELATED_LABEL'), {'fields': [
               # 'autor',
                'categories',
                'album',
                'tags',
            ]}),
        (_('BASIC_SEO'), {'fields': [
                'slug',
                'meta_title',
                'meta_description',
            ]}),
    ]
    form = DirectoryItemAdminForm
    prepopulated_fields = {"slug": ("title",),}

create_modeladmin(DirectoryCategoryAdmin, name='directory-category', model=DirectoryCategory) 
#create_modeladmin(DirectoryItemAdmin, name='directory_item', model=DirectoryItem) 
