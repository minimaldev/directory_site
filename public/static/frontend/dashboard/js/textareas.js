
(function($)
{ 
  $(function()
  {
    $(document).ready(function()
    {
      elem =  document.getElementById("id_content")

      function OpenMediaImageToolbar(editor) 
      {
        window.selector_id="content";
        url = elem.getAttribute('data-media-image-url')
        var popup = window.open(url ,"Una", "scrollbars=1,resizable=0,height=640,width=640");
        popup.focus();

      }
      function OpenMediaAlbumToolbar(editor) 
      {
        window.selector_id="content";
        url = elem.getAttribute('data-media-album-url')
        var popup = window.open(url ,"Dos", "scrollbars=1,resizable=0,height=640,width=640");
        popup.focus();

      }

      var shortcuts = {
        "toggleBold": "Cmd-B",
        "toggleItalic": "Cmd-I",
        "drawLink": "Cmd-K",
        "toggleHeadingSmaller": "Cmd-H",
        "toggleHeadingBigger": "Shift-Cmd-H",
        "cleanBlock": "Cmd-E",
        "drawImage": "Cmd-Alt-I",
        "toggleBlockquote": "Cmd-'",
        "toggleOrderedList": "Cmd-Alt-L",
        "toggleUnorderedList": "Cmd-L",
        "toggleCodeBlock": "Cmd-Alt-C",
        "togglePreview": "Cmd-P",
        "toggleSideBySide": "F9",
        "toggleFullScreen": "F11"
      };




      window.myEditor = new SimpleMDE({
        toolbar: [
          {
              name: "OpenMediaImageToolbar",
              action: OpenMediaImageToolbar,
              className: "fa fa-camera-retro", // Look for a suitable icon
              title: "Open Media Image Toolbar (Ctrl/Cmd-Alt-T)",
          },
          {
              name: "OpenMediaAlbumToolbar",
              action: OpenMediaAlbumToolbar,
              className: "fa fa-camera", // Look for a suitable icon
              title: "Open Media Album Toolbar (Ctrl/Cmd-Alt-T)",
          },

          {
              name: "toggleBold",
              action: SimpleMDE.toggleBold,
              className: "fa fa-bold", // Look for a suitable icon
              title: "bold (Cmd-B)",
          },
          {
              name: "toggleItalic",
              action: SimpleMDE.toggleItalic,
              className: "fa fa-italic", // Look for a suitable icon
              title: "italic (Cmd-I)",
          },
          {
              name: "drawLink",
              action: SimpleMDE.drawLink,
              className: "fa fa-link", // Look for a suitable icon
              title: "link (Cmd-K)",
          },
          {
              name: "toggleHeadingSmaller",
              action: SimpleMDE.toggleHeadingSmaller,
              className: "fa fa fa-header", // Look for a suitable icon
              title: "heading-smaller",
          },
          {
              name: "toggleHeadingBigger",
              action: SimpleMDE.toggleHeadingBigger,
              className: "fa fa fa-header", // Look for a suitable icon
              title: "heading-bigger",
          },
          {
              name: "cleanBlock",
              action: SimpleMDE.cleanBlock,
              className: "fa fa-eraser fa-clean-block", // Look for a suitable icon
              title: "clean-block",
          },
          {
              name: "drawImage",
              action: SimpleMDE.drawImage,
              className: "fa fa-picture-o", // Look for a suitable icon
              title: "image",
          },
          {
              name: "toggleBlockquote",
              action: SimpleMDE.toggleBlockquote,
              className: "fa fa-quote-left", // Look for a suitable icon
              title: "quote",
          },
          {
              name: "toggleOrderedList",
              action: SimpleMDE.toggleOrderedList,
              className: "fa fa-list-ol", // Look for a suitable icon
              title: "link",
          },
          {
              name: "toggleUnorderedList",
              action: SimpleMDE.toggleUnorderedList,
              className: "fa fa-list-ul", // Look for a suitable icon
              title: "ordered-list",
          },
          {
              name: "toggleUnorderedList",
              action: SimpleMDE.toggleUnorderedList,
              className: "fa fa-list-ul", // Look for a suitable icon
              title: "unordered-list",
          },
          {
              name: "toggleCodeBlock",
              action: SimpleMDE.toggleCodeBlock,
              className: "fa fa-code", // Look for a suitable icon
              title: "code",
          },
          {
              name: "togglePreview",
              action: SimpleMDE.togglePreview,
              className: "fa fa-eye", // Look for a suitable icon
              title: "preview",
          },
         /* {
              name: "toggleSideBySide",
              action: SimpleMDE.toggleSideBySide,
              className: "fa fa-columns", // Look for a suitable icon
              title: "side-by-side",
          },
          {
              name: "toggleFullScreen",
              action: SimpleMDE.toggleFullScreen,
              className: "fa fa-arrows-alt", // Look for a suitable icon
              title: "fullscreen",
          }      
          */



        ],
       
        element:elem,
        spellChecker: false,
      });
      /**
       Agregar en el futuro un boton toolbar
      */
    
    })
  });
})(django.jQuery);

