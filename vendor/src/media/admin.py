# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from media.models import MediaAlbum
from media.models import MediaImage

class MediaImageInline(admin.StackedInline):
    model = MediaImage.album.through # or what ever your models name is
    extra = 5 # or how ever many you want to add at a time

class MediaAlbumAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_display = ['album_title', 'created', 'modified']
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': ['title']}),
    ]
    inlines = [MediaImageInline,]


class MediaImageAdmin(admin.ModelAdmin):

    search_fields = ['title']
    list_display = (
        'title',
        'thumb',       
        #'rating',

        'get_albums',
        'Tags',
  
        'created',
        'modified'
        )
    list_filter = ['tags', 'album']

admin.site.register(MediaAlbum, MediaAlbumAdmin)
admin.site.register(MediaImage, MediaImageAdmin)