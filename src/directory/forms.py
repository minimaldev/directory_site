from django import forms
from media.widgets import MediaItemWidget, MediaContentWidget
from directory.models import DirectoryItem
from directory.models import DirectoryCategory
from media.models import MediaAlbum
from media.models import MediaImage
from media.models import MediaVideos
from utilities.forms import MultiCharField
from mptt.forms import TreeNodeChoiceField
from django.utils import timezone
from datetime import timedelta
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
import json
import redis
import itertools

r = redis.StrictRedis(host='localhost', port=6379, db=0)

class SearchForm(forms.Form):
	q = forms.CharField(max_length=255)
	def __init__(self, *args, **kwargs):
		super(SearchForm, self).__init__(*args, **kwargs)
		self.fields['q'].required = False

DAYS_DATA = (
    (7, '7'),
    (12, '12'),
    (30, '30'),
    (60, '60'),
    (90, '90'),
)

class DirectoryItemForm(forms.ModelForm):
#    featured_days = forms.ChoiceField(
#        label=_('FEATURED_LABEL'),
#        #default='7', 
#        choices=DAYS_DATA,
#        widget=forms.HiddenInput()
#        #max_length=2
#    )   
    release_date_days = forms.ChoiceField(
        #default='7', 
        label=_('SHOW_ADVERT_UNTIL_LABEL'),        
        choices=DAYS_DATA,
        widget=forms.HiddenInput()#,
        #max_length=2
    ) 
    GUID = forms.CharField(
        max_length=1850, 
        widget=forms.HiddenInput()
    )   
    def __init__(self, data=None, files=None, request=None, *args, **kwargs):
        self._user_site = kwargs.pop('user_site', None)
        self._request = kwargs.pop('request', None)
        self._is_new = kwargs.pop('is_new', False)
        super(DirectoryItemForm, self).__init__(data=data, files=files, *args, **kwargs) 
        for key in [ 
            'title',  'content', 
            'price', 'phone', 'publish_date']:
            self.fields[key].required = True 
  
    class Meta:
        model = DirectoryItem
        exclude = ('created','release_date','modified','user')

    def save(self, commit=True): 

        m = super(DirectoryItemForm, self).save(commit=False)
        #featured_days = int(self.cleaned_data['featured_days'])
        release_date_days = int(self.cleaned_data['release_date_days'])
        GUID = self.cleaned_data['GUID']
        if commit:            
            m.user = self._user_site 
            m.publish = True
            if self._is_new:   
                m.release_date = timezone.now() + timedelta(days=release_date_days)
                #m.featured_end_date = timezone.now() + timedelta(days=featured_days)
                m.save()  
                messages.add_message(
                    self._request, 
                    messages.INFO, 
                    _('ADVERT_SUCCESFULLY_CREATED_LABEL')
                )
            else:               
                if m._release_date_days is not None :   
                    
                    sustract = m.release_date - timedelta(days=m._release_date_days) 
                    if m._release_date_days != release_date_days:
                        new_release_date_days = sustract + timedelta(days=release_date_days) 
                        m.release_date = new_release_date_days
                else:
                    m.release_date = timezone.now() + timedelta(days=release_date_days)

                #if m._featured_days is not None:
                #    sustract = m.release_date - timedelta(days=m._featured_days) 
                #    if m._featured_days != featured_days:
                #        new_release_date_days = sustract + timedelta(days=featured_days) 
                #        m.featured_end_date = new_release_date_days
                #else:
                #    m.featured_end_date = timezone.now() + timedelta(days=featured_days)

                m.save()
                messages.add_message(
                    self._request, 
                    messages.INFO, 
                    _('ADVERT_SUCCESFULLY_UPDATED_LABEL')
                )
            jsons = r.get('images_%s' % (GUID))
            
            if jsons is not None:
                unpacked_images = json.loads(jsons) 
                       
                if len(unpacked_images) > 0:
                    A, Status = MediaAlbum.objects.get_or_create(
                        title='advert-%s'%(m.id)
                    )
                  
                    m.album.add(A.id)

                    for image in unpacked_images:
                        for I in MediaImage.objects.filter(id=image.get('id')):
                            I.album.add(A.id)  
                            print "album saved"
                    r.delete('images_%s' % (GUID))
            # save videos
            jsons_videos = r.get('videos_%s' % (GUID))
            if jsons_videos is not None:
                unpacked_videos = json.loads(jsons_videos)
                if len(unpacked_videos) > 0:
                    A, Status = MediaAlbum.objects.get_or_create(
                            title='advert-%s'%(m.id)
                        )
                    m.album.add(A.id)
                    print "album saved video"
                    for video in unpacked_videos:
                        for V in MediaVideos.objects.filter(id=video.get('id')):
                            V.album.add(A.id)
                    r.delete('videos_%s' % (GUID))

        return m

class DirectoryItemAdminForm(forms.ModelForm):

    class Meta:  
        widgets = {
            'content':  MediaContentWidget(attrs={ 'v-model':"input",'debounce':"300"}),
        	'thumbnail': MediaItemWidget(),
        	'featured_image': MediaItemWidget(), 
            'meta_description':  forms.Textarea,        
        }
    
class DirectoryCategoryAdminForm(forms.ModelForm):
    parent = TreeNodeChoiceField( DirectoryCategory.objects.all(
              
            ),required=False)
    class Meta:  
        widgets = {
          'meta_description':  forms.Textarea(
            attrs={
                'maxlength':156,
                #'data-url': reverse("navigation_get_item"),
            }),
        }

    def __init__(self, *args, **kwargs):
        super(DirectoryCategoryAdminForm, self).__init__(*args, **kwargs)
        self.fields['parent'].queryset = DirectoryCategory.objects.all(
            ).exclude(
                id__exact=self.instance.id,
            )

