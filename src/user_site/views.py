import json
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.utils.http import is_safe_url
from django.template.response import TemplateResponse
from django.utils.translation import ugettext as _
from django.shortcuts import resolve_url
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import REDIRECT_FIELD_NAME
from user_site import login as auth_login, logout as auth_logout
from user_site.decorators import login_required
from django.contrib.sites.models import get_current_site
from django.utils.http import base36_to_int, is_safe_url, urlsafe_base64_decode, urlsafe_base64_encode

from django.shortcuts import get_object_or_404
from user_site.forms import AuthenticationForm
from user_site.forms import PasswordChangeForm
import importlib

if hasattr(settings, 'AUTH_USER_SITE_PROFILE_FORM'):    
    app_label, className = settings.AUTH_USER_SITE_PROFILE_FORM.split('.')
    module = importlib.import_module(app_label)
    UserSiteProfileForm = getattr(module, className)
else:
    from user_site.forms import UserSiteProfileForm


from user_site.forms import SetPasswordForm2
from user_site.forms import PasswordResetForm
from django.contrib import messages
from user_site import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import RequestSite
from user_site.registration.forms import RegistrationForm
from user_site.registration import signals
from user_site.registration.models import RegistrationProfile
from user_site.forms import UserSiteProfileImagesForm

@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(
        request, 
        template_name='frontend/modules/user_site/registration/login.html',
        redirect_field_name=REDIRECT_FIELD_NAME,
        authentication_form=AuthenticationForm,
        current_app=None, 
        extra_context=None):
  
    if request.user_site.is_authenticated():
        auth_redirect = reverse('home')
        return HttpResponseRedirect(auth_redirect)
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    context = {
            
            redirect_field_name: redirect_to,
            #'site': current_site,
            #'site_name': current_site.name,
        }
    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
       
        if form.is_valid():
            auth_redirect = reverse('home')
            if  is_safe_url(url=redirect_to, host=request.get_host()):
                auth_redirect = resolve_url(redirect_to)       
            auth_login(request, form.get_user())
            return HttpResponseRedirect(auth_redirect)
        else:
            context['errors'] = []
            for key, error in form.errors.iteritems():
                data = {}
                c = key.capitalize()
                data[c] = []
                context['errors'].append(data)
                for i in error:
                    data[c].append(i)            
    else:
        form = authentication_form(request)

    context['form'] = form

    
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(
            request, 
            template_name, 
            context,
            current_app=current_app
        )

def logout(
        request, 
        next_page=None,
        template_name='frontend/modules/user_site/registration/logged_out.html',
        redirect_field_name=REDIRECT_FIELD_NAME,
        current_app=None, 
        extra_context=None):
    auth_logout(request)

    if next_page is not None:
        next_page = resolve_url(next_page)

    if redirect_field_name in request.REQUEST:
        next_page = request.REQUEST[redirect_field_name]
        if not is_safe_url(url=next_page, host=request.get_host()):
            next_page = request.path

    if next_page:        
        return HttpResponseRedirect(next_page)

    #current_site = get_current_site(request)
    context = {
    #    'site': current_site,
    #    'site_name': current_site.name,
        'title': _('Logged out')
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
        current_app=current_app)
####vamos por aca abajo

@sensitive_post_parameters()
@csrf_protect
@login_required
def password_change(
            request,
            template_name='frontend/modules/user_site/registration/password_change_form.html',
            post_change_redirect=None,
            password_change_form=PasswordChangeForm,
            current_app=None, 
            extra_context=None):
    if not request.user_site.is_authenticated():
        if post_change_redirect is None:
            post_change_redirect = reverse('user_site_auth_password_reset_done')
        else:
            post_change_redirect = resolve_url(post_reset_redirect)
    else:
        post_change_redirect = reverse('user_site_auth_profile')
    #if post_change_redirect is None:
    #    post_change_redirect = reverse('user_site_auth_password_change_done')
    #else:
    #    post_change_redirect = resolve_url(post_change_redirect)
    if request.method == "POST":
        form = password_change_form( user_site=request.user_site, data=request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, _('Password Changed successfully'))
            return HttpResponseRedirect(post_change_redirect)
    else:

        form = password_change_form(user_site=request.user_site)
    context = {
        'form': form,
        'my_profile': True,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)



@csrf_protect
def password_reset(request, is_admin_site=False,
                   template_name='frontend/modules/user_site/registration/password_reset_form.html',
                   email_template_name='frontend/modules/user_site/registration/email/password_reset_email.html',
                   subject_template_name='frontend/modules/user_site/registration/email/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   current_app=None,
                   extra_context=None):
  
    if post_reset_redirect is None:
        post_reset_redirect = reverse('user_site_auth_password_reset_done')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
            }
            if is_admin_site:
                opts = dict(opts, domain_override=request.get_host())
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)
    else:
        form = password_reset_form()
    context = {
        'form': form,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

def password_reset_done(request,
                        template_name='frontend/modules/user_site/registration/password_reset_done.html',
                        current_app=None, extra_context=None):
    context = {}
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


def password_reset_complete(request,
                            template_name='frontend/modules/user_site/registration/password_reset_complete.html',
                            current_app=None, extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL)
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='frontend/modules/user_site/registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm2,
                           post_reset_redirect=None,
                           current_app=None, extra_context=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse('user_site_auth_password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(None)
    else:
        validlink = False
        form = None
    context = {
        'form': form,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

@login_required
def password_change_done(
            request,
            template_name='frontend/modules/user_site/registration/password_change_done.html',
            current_app=None, 
            extra_context=None):
    context = {}
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

@sensitive_post_parameters()
@csrf_protect
@login_required
def my_profile(request, template_name='dashboard/modules/user_site/profile.html'):
    context = {}
    context['my_profile'] = True    

    if request.method == "POST":
        x = request.POST
        context['profile_form'] = UserSiteProfileForm(
                instance=request.user_site,
                data=x,
                files=request.FILES
            )
        
        if context['profile_form'].is_valid():
            context['profile_form'].save()
            messages.add_message(request, messages.INFO, _('Your Personal information has changed successfully'))
            post_change_redirect = reverse('user_site_auth_profile')
            return HttpResponseRedirect(post_change_redirect)
    else:
        context['profile_form'] = UserSiteProfileForm(instance=request.user_site)

    context['password_change_form'] = PasswordChangeForm(user_site=request.user_site)
    return TemplateResponse(request, template_name, context)

@sensitive_post_parameters()
@csrf_protect
@never_cache
def login_ajax(
        request, 
        authentication_form=AuthenticationForm):
    context = {
        'success': True
    } 
    if request.user_site.is_authenticated():
        return HttpResponse(json.dumps(context), 
            content_type="application/json")

    if request.method == "POST":
        
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
        else:
            context['success'] = False
            context['errors'] = []
            fields = form.fields
            for key, error in form.errors.iteritems():
                data = {}
                if (fields.has_key(key)):
                    c = str(fields[key].label)
                else:
                    c = str(key)
                data[c] = []
                context['errors'].append(data)
                for i in error:
                    data[c].append(i)

        

    return HttpResponse(json.dumps(context), content_type="application/json")

@csrf_protect
@never_cache
def registration_ajax(request):
    context = {
        'success': True
    } 
    if request.user_site.is_authenticated():
        return HttpResponse(json.dumps(context), content_type="application/json")

    if request.method == "POST":
        
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            email, password =form.cleaned_data['email'], form.cleaned_data['password1']
            site = RequestSite(request)
            new_user = RegistrationProfile.objects.create_inactive_user(
                    email=email,
                    password=password,
                    
                )
            signals.user_site_registered.send(
                    sender=None,
                    user_site=new_user,
                    request=request
                )
            context['message'] = _("An activation link has been sent please check your email inbox")
        else:
            context['success'] = False
            
            context['errors'] = []
            fields = form.fields
            for key, error in form.errors.iteritems():
                data = {}
                if (fields.has_key(key)):
                    c = str(fields[key].label)
                else:
                    c = str(key)
                data[c] = []
                context['errors'].append(data)
                for i in error:
                    data[c].append(i)
    else:
        context['success'] = False

    return HttpResponse(json.dumps(context), content_type="application/json")

@csrf_protect
@never_cache
@login_required
def add_item_profile_image(request):
    context = {'success': False}
    if request.method == "POST":
        x = request.POST
        form = UserSiteProfileImagesForm(
            instance=request.user_site,
            data=x,
            files=request.FILES
        )
        if form.is_valid():
            form.save()

            context['success'] = True
            context['logo'] = request.user_site.get_logo(250,250)
    else:
        form = UserSiteProfileImagesForm(instance=request.user_site)

    return HttpResponse(json.dumps(context), content_type="application/json")

def user_is_autenticated(request):
    context = {'success': False}
    if request.user_site.is_authenticated():
        context['success'] = True

    return HttpResponse(json.dumps(context), content_type="application/json")