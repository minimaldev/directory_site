from django import forms
from newsletter.models import EmailNewsletter

class EmailNewsletterCreationModelForm(forms.ModelForm):
    
    class Meta:  
    	model = EmailNewsletter
        exclude = ('suscribed','created','modified',)

class EmailNewsletterUnsuscribeModelForm(forms.ModelForm):
    class Meta:  
    	model = EmailNewsletter
    	exclude = ('created','modified',)
      
