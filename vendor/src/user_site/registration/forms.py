from user_site.models import UserSite as User
from django import forms
from django.utils.translation import ugettext_lazy as _

class RegistrationForm(forms.Form):

    required_css_class = 'required'


    email = forms.EmailField(label=_("E-mail"))

    password1 = forms.CharField(
                    widget=forms.PasswordInput,
                    label=_("Password")
                )

    password2 = forms.CharField(
                    widget=forms.PasswordInput,
                    label=_("Repeat Password")
                )
    
    def clean_email(self):
        existing = User.objects.filter(email__iexact=self.cleaned_data['email'])
        if existing.exists():
            raise forms.ValidationError(_("A user with that email already exists."))
        else:
            return self.cleaned_data['email']


    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields didn't match."))
        return self.cleaned_data


class RegistrationFormTermsOfService(RegistrationForm):

    tos = forms.BooleanField(
                widget=forms.CheckboxInput,
                label=_(u'I have read and agree to the Terms of Service'),
                error_messages={'required': _("You must agree to the terms to register")}
            )


class RegistrationFormUniqueEmail(RegistrationForm):

    def clean_email(self):

        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(
                _("This email address is already in use. Please supply a different email address.")
            )
        return self.cleaned_data['email']


class RegistrationFormNoFreeEmail(RegistrationForm):

    bad_domains = [
        'aim.com', 'aol.com', 'email.com', 'gmail.com',
        'googlemail.com', 'hotmail.com', 'hushmail.com',
        'msn.com', 'mail.ru', 'mailinator.com', 'live.com',
        'yahoo.com'
    ]
    
    def clean_email(self):
        email_domain = self.cleaned_data['email'].split('@')[1]
        if email_domain in self.bad_domains:
            raise forms.ValidationError(
                _("Registration using free email addresses is prohibited. Please supply a different email address.")
            )
        return self.cleaned_data['email']
