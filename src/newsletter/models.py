# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from utilities.models import BaseDateTime

class EmailNewsletter(BaseDateTime):

    suscribed = models.BooleanField(
            _('SUSCRIBED_LABEL'),
            default=False,
        )

    email = models.EmailField(
            _('EMAIL_LABEL')
        )

    def __unicode__(self):
        return self.email

    class Meta:
        abstract = True
        verbose_name = _('NEWSLETTER_TITLE')
        verbose_name_plural = _('NEWSLETTER_TITLE_PLURAL')
        get_latest_by = 'created'
        ordering = ('-id',)
        db_table = 'newsletter_emails'
        app_label = 'newsletter'
