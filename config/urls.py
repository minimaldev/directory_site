from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from utilities.generic.base import TemplateView
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^terms.html$',
        TemplateView.as_view(
            template_name='frontend/modules/pages/terms.html', 
            content_type='text/html'
        ), 
        name='terms'
    ), 

    url(r'^about-us.html$',
        TemplateView.as_view(
            template_name='frontend/modules/pages/about_us.html', 
            content_type='text/html'
        ), 
        name='about_us'
    ), 

    url(r'^privacy.html$',
        TemplateView.as_view(
            template_name='frontend/modules/pages/privacy_policy.html', 
            content_type='text/html'
        ), 
        name='privacy_policy'
    ), 
    
    url(r'^contact/', include("contact_form.urls")),

    url(r'^account/', include('src.user_site.urls')),
    url(r'^account/', include('src.user_site.registration.urls')),
    url(r'^', include('src.directory.urls')),  
    url(r'^account/', include('src.directory.auth_urls')),   
    url(r'^account/', include('src.social_auth.urls')),
    url(r'^account/social/', include('src.user_site.social.urls')),
    url(r'^admin/kawai/', include(admin.site.urls)),
    url(r'^sitemap/', include("sitemap.urls")),
    url(r'^mediaframework/', include("media.urls")),
    url(r'^newsletter/', include("newsletter.urls")),
    url(r'^', include("globaly.urls")),
    url(r'^', include("rss.urls")),
    
    url(r'^$',
        TemplateView.as_view(
            template_name='landing.html', 
            content_type='text/html'
        ), 
        name='home'
    ), 

    url(r'^(?P<page>\d+)/{0,1}$',
        TemplateView.as_view(
            template_name='index.html', 
            content_type='text/html'
        ), 
        name='home'
    ), 


    
    url(r'^(?P<slug>[-\w\d]+)/(?P<page>\d+)/{0,1}$',
        TemplateView.as_view(
         template_name='index.html', 
         content_type='text/html'
        ), 
        name='page_details'
    ),

    url(r'^(?P<slug>[-\w\d]+)/{0,1}$',
        TemplateView.as_view(
         template_name='index.html', 
         content_type='text/html'
        ), 
        name='page_details'
    ),

    url(r'^robots\.txt$', 
        TemplateView.as_view(
            template_name='robots.txt', 
            content_type= 'text/plain'
        ),
        name='robots'
    ),


    url(r'^robots\.txt$', 
        TemplateView.as_view(
            template_name='robots.txt', 
            content_type= 'text/plain'
        ),
        name='robots'
    ),

    
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = "globaly.views.handler404"
handler500 = "globaly.views.handler500"
