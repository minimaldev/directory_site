from django.conf import settings
from django import template
register = template.Library()

from newsletter.forms  import EmailNewsletterCreationModelForm
from newsletter.forms  import EmailNewsletterUnsuscribeModelForm

@register.assignment_tag(name='EmailNewsletterCreationForm', takes_context=True)
def EmailNewsletterCreationForm(context):
    request = context['request']
    return EmailNewsletterCreationModelForm()

@register.assignment_tag(name='EmailNewsletterUnsuscribeForm', takes_context=True)
def EmailNewsletterUnsuscribeForm(context):
    request = context['request']
    return EmailNewsletterUnsuscribeModelForm()
