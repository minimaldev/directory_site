from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls import patterns
from src.user_site.social import views

urlpatterns = patterns('',

	url(r'^login/facebook/$',
        views.login_facebook,
        name='user_site_auth_login_facebook'
    ),
    
    url(r'^login/facebook/complete/$',
        views.login_facebook_complete,
        name='user_site_auth_login_facebook_complete'
    ),

   	url(r'^login/twitter/$',
        views.login_twitter,
        name='user_site_auth_login_twitter_login'
    ),
    
    url(r'^login/twitter/complete/$',
        views.login_twitter_complete,
        name='user_site_auth_login_twitter_complete'
    ),

    url(r'^login/instagram/$',
        views.login_instagram,
        name='user_site_auth_login_instagram_login'
    ),

    url(r'^login/instagram/complete/$',
        views.login_instagram_complete,
        name='user_site_auth_login_instagram_complete'
    ),
)