import oauth2 as oauth
import time
import StringIO
import pycurl
import urllib
import urllib2
import requests
import urlparse
import json  as simplejson
from instagram.client import InstagramAPI
import cgi
from django.conf import settings
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse, NoReverseMatch
from user_site.models import UserSite
from user_site.decorators import login_required
from urllib import urlencode
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from utilities.context_processors import get_domain
from requests_oauthlib import OAuth1
import json
import facebook


#from src.user_site.social.models import UserSiteSocialProfile, create_profile
#from src.user_site import login, authenticate

FACEBOOK_ACCESS_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token'
FACEBOOK_REQUEST_TOKEN_URL = 'https://www.facebook.com/dialog/oauth'
FACEBOOK_CHECK_AUTH = 'https://graph.facebook.com/me'

TWITTER_SERVER = 'api.twitter.com'
TWITTER_REQUEST_TOKEN_URL = 'https://%s/oauth/request_token' % TWITTER_SERVER
TWITTER_ACCESS_TOKEN_URL = 'https://%s/oauth/access_token' % TWITTER_SERVER
TWITTER_AUTHORIZATION_URL = 'https://%s/oauth/authenticate' % TWITTER_SERVER
TWITTER_CHECK_AUTH = 'https://%s/1.1/account/verify_credentials.json' % TWITTER_SERVER

twitter_consumer = oauth.Consumer(
    settings.TWITTER_CONSUMER_KEY, 
    settings.TWITTER_CONSUMER_SECRET
)

INSTAGRAM_SERVER = 'instagram.com'
INSTAGRAM_AUTHORIZATION_URL = 'https://instagram.com/oauth/authorize'
INSTAGRAM_ACCESS_TOKEN_URL = 'https://api.instagram.com/oauth/access_token'
INSTAGRAM_CHECK_AUTH = 'https://api.instagram.com/v1/users/self'

instagram_consumer = oauth.Consumer(
    settings.INSTAGRAM_CONSUMER_KEY,
    settings.INSTAGRAM_CONSUMER_SECRET
)

#http://localhost:8000/account/social/login/instagram/complete/
def login_facebook(request):
    callback_url = get_domain(request).get('current_domain') + reverse('user_site_auth_login_facebook_complete')
    #print callback_url
    return HttpResponseRedirect(
        FACEBOOK_REQUEST_TOKEN_URL + '?client_id=%s&redirect_uri=%s&scope=%s' 
        % (settings.FACEBOOK_APP_ID,  urllib.quote_plus(callback_url), 'public_profile')
    )

def login_facebook_complete(request):
    """ Gets the access token and profile info from facebook """
    code = request.GET.get('code')
    consumer = oauth.Consumer(key=settings.FACEBOOK_APP_ID, secret=settings.FACEBOOK_API_SECRET)
    client = oauth.Client(consumer)
    redirect_uri = get_domain(request).get('current_domain') + reverse('user_site_auth_login_facebook_complete')
    request_url = FACEBOOK_ACCESS_TOKEN_URL + '?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s' % (settings.FACEBOOK_APP_ID, redirect_uri, settings.FACEBOOK_API_SECRET, code) 
    resp, content = client.request(request_url, 'GET')
    access_token = dict(urlparse.parse_qsl(content))

    if access_token.has_key('access_token'):
        graph = facebook.GraphAPI(access_token=access_token['access_token'], version='2.2')
        request_url = FACEBOOK_CHECK_AUTH + '?access_token=%s' % access_token['access_token']
        args = {'fields' : 'id,name,about', }
        profile = graph.get_object('me', **args)
        response = graph.get_object('me/picture', type='large')
      
        UserSite.objects.filter(
            id=request.user_site.id
            ).update(           
            logo=request.user_site.download_logo(response['url']),           
        )  
        #print response['url']
        #if resp['status'] == '200':
        #    resp, content = client.request(request_url, 'GET')
        #    content_dict = json.loads(content)
        #    print content_dict
        #    #userid = content_dict['id']
        #    #username = (content_dict.get('username') or (content_dict.get('first_name') + content_dict.get('last_name')))
           
    return HttpResponseRedirect(reverse('user_site_auth_profile'))

@login_required
def login_twitter(request):
    # Step 1. Get a request token from Twitter.
    twitter_client = oauth.Client(twitter_consumer)
    resp, content = twitter_client.request(
        TWITTER_REQUEST_TOKEN_URL, 
        "GET"
    )
    if resp['status'] != '200':
        raise Exception("Invalid response from Twitter.")

    # Step 2. Store the request token in a session for later use.
    request.session['request_token'] = dict(cgi.parse_qsl(content))

    # Step 3. Redirect the user to the authentication URL.
    url = "%s?oauth_token=%s" % (TWITTER_AUTHORIZATION_URL,
        request.session['request_token']['oauth_token'])

    return HttpResponseRedirect(url)

@login_required
def login_twitter_complete(request):
    # Step 1. Use the request token in the session to build a new client.
    token = oauth.Token(
        request.session['request_token']['oauth_token'],
        request.session['request_token']['oauth_token_secret']
    )
    token.set_verifier(request.GET['oauth_verifier'])
    twitter_client = oauth.Client(
        twitter_consumer, 
        token
    )

    # Step 2. Request the authorized access token from Twitter.
    resp, content = twitter_client.request(
        TWITTER_ACCESS_TOKEN_URL,
        "GET"
    )
    if resp['status'] != '200':
        print content
        raise Exception("Invalid response from Twitter.")

    """
    This is what you'll get back from Twitter. Note that it includes the
    user's user_id and screen_name.
    {
        'oauth_token_secret': 'IcJXPiJh8be3BjDWW50uCY31chyhsMHEhqJVsphC3M',
        'user_id': '120889797', 
        'oauth_token': '120889797-H5zNnM3qE0iFoTTpNEHIz3noL9FKzXiOxwtnyVOD',
        'screen_name': 'heyismysiteup'
    }
    """
    
    access_token = dict(cgi.parse_qsl(content))

    auth = OAuth1(
        settings.TWITTER_CONSUMER_KEY, 
        settings.TWITTER_CONSUMER_SECRET,
        access_token['oauth_token'], 
        access_token['oauth_token_secret']
    )
    r = requests.get(TWITTER_CHECK_AUTH, auth=auth)
    data = r.json()
    description = data['description']
    if len(description) == 0:
        description = request.user_site.description
    UserSite.objects.filter(
        id=request.user_site.id
    ).update(
        twitter_user_id=access_token['user_id'],
        nick=data['screen_name'],
        logo=request.user_site.download_logo(data['profile_image_url_https']),
        description=description
    )
    messages.add_message(request, messages.INFO, _('You are sucessfully Connected to twitter'))

    return HttpResponseRedirect(reverse('user_site_auth_profile'))


@login_required
def login_instagram(request):

    instagram_client = InstagramAPI(
        client_id=settings.INSTAGRAM_CONSUMER_KEY, 
        client_secret=settings.INSTAGRAM_CONSUMER_SECRET, 
        redirect_uri=get_domain(request).get('current_domain') + reverse('user_site_auth_login_instagram_complete')
    )

    return HttpResponseRedirect(instagram_client.get_authorize_url(scope=['basic','follower_list']))

@login_required
def login_instagram_complete(request):
    if request.GET.get('code') is not None:
        instagram_client = InstagramAPI(
            client_id=settings.INSTAGRAM_CONSUMER_KEY, 
            client_secret=settings.INSTAGRAM_CONSUMER_SECRET, 
            redirect_uri=get_domain(request).get('current_domain') + reverse('user_site_auth_login_instagram_complete')
        )
        access_token, instagram_user = instagram_client.exchange_code_for_access_token(
            request.GET.get('code')
        )
        if not access_token:
            print 'Could not get access token'
        else:
            #print instagram_user
            #print instagram_user['id']
            
            description = instagram_user['bio']
            if len(description) == 0:
                description = request.user_site.description
            UserSite.objects.filter(
                id=request.user_site.id
            ).update(
                instagram_user_id=instagram_user['id'],
                instagram_token=access_token,
                logo=request.user_site.download_logo(instagram_user['profile_picture']),
                nick=instagram_user['username'],
                description=description

            )
            messages.add_message(request, messages.INFO, _('You are sucessfully Connected to istagram'))

    return HttpResponseRedirect(reverse('user_site_auth_profile'))