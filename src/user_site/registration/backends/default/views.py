from django.conf import settings
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site
from user_site.registration import signals
from user_site.registration.models import RegistrationProfile
from user_site.registration.views import ActivationView as BaseActivationView
from user_site.registration.views import RegistrationView as BaseRegistrationView

class RegistrationView(BaseRegistrationView):

    def register(self, request, **cleaned_data):
        email, password = cleaned_data['email'], cleaned_data['password1']
       
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        new_user = RegistrationProfile.objects.create_inactive_user(
                email=email,
                password=password
            )
        signals.user_site_registered.send(
                sender=self.__class__,
                user_site=new_user,
                request=request
            )
        return new_user

    def registration_allowed(self, request):
        return getattr(settings, 'REGISTRATION_OPEN', True)

    def get_success_url(self, request, user):
        return ('user_site_registration_complete', (), {})

class ActivationView(BaseActivationView):
    def activate(self, request, activation_key):
        activated_user = RegistrationProfile.objects.activate_user(activation_key)
        if activated_user:
            signals.user_site_activated.send(
                sender=self.__class__,
                user_site=activated_user,
                request=request
            )
        return activated_user

    def get_success_url(self, request, user):
        return ('user_site_registration_activation_complete', (), {})
