from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from user.models import  CustomUser

def get_user_model():
    """
    Returns the User model that is active in this project.
    """
    try:
        return CustomUser
    except ValueError:
        raise ImproperlyConfigured("AUTH_USER_MODEL must be of the form 'app_label.model_name'")
    except LookupError:
        raise ImproperlyConfigured(
            "AUTH_USER_MODEL refers to model '%s' that has not been installed" % settings.AUTH_USER_MODEL
        )

