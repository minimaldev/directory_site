# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import GlobalConfig
class GlobalConfigAdmin(admin.ModelAdmin):
    list_display = ('sitename', 'sitelink','created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': ['sitename', 'sitelink','meta_title', 'meta_description']}),
    ]

admin.site.register(GlobalConfig, GlobalConfigAdmin) 