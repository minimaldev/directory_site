(function($) 
{ 
	$(window).load(function()
	{
		function openpopup(event)
		{
		 	event.preventDefault();
		 	var selector=$(this);
		 	window.selector_id=$(this).attr('id');
	  		var popup = window.open(selector.attr('href') ,"Una", "scrollbars=1,resizable=0,height=640,width=640");
	  		popup.focus();
	  	}
	  	$('.selectmediaItem').click(openpopup);
	});
})(django.jQuery);