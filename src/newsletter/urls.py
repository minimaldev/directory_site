from django.conf.urls import patterns
from django.conf.urls import url
#from utilities.generic.base import TemplateView
from newsletter import views 
# Create your views here.
urlpatterns = patterns('',

    url(r'^suscribe/{0,1}$',
            views.suscribe,
            name='newsletter_suscribe'
        ),

    url(r'^unsuscribe/{0,1}$',
            views.unsuscribe,
            name='newsletter_unsuscribe'
        ),
)