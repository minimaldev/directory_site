# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from globaly.models import GlobalyTags
from globaly.forms import GlobalyUrlsInline, GlobalyTagsAdminForm
from globaly.models import GlobalyUrls

class GlobalyTagsAdmin(admin.ModelAdmin):
    class Media:
        js = ('/static/js/copytitle.js',)
    list_display = ('name', 'created', 'modified')
    fieldsets = [
        (_('BASIC_LABEL'), {'fields': [ 'name']}),
        (_('BASIC_SEO'), {'fields': [
            'slug',
            'meta_title',
            'meta_description',
         ]}),          
    ]
    prepopulated_fields = {"slug": ("name",),}
    form = GlobalyTagsAdminForm

admin.site.register(GlobalyTags, GlobalyTagsAdmin) 