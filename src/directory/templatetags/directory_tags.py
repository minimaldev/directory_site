import itertools
from django.conf import settings
from django import template
from django.utils import timezone
#from comments.models import Comments
#from comments.forms import CommentsModelForm
from django.db.models import Q
register = template.Library()
from directory.models import DirectoryCategory
from directory.models import DirectoryItem
from django.utils import timezone
from hurry.filesize import size
from django.db import models
from random import randint,sample, choice
from django.db.models import Max
from django.utils.translation import ugettext_lazy as _
from media.models import MediaAlbum
from media.models import MediaImage, MediaVideos

@register.assignment_tag(name='get_featured_items_category_id', takes_context=True)
def get_featured_items_category_id(context, category_id,  limit=3):
    request = context['request']
    current_date = timezone.now()
    return DirectoryItem.objects.filter(
            categories__id=category_id#,
            #featured_start_date__lte=current_date
        ).order_by("-featured_start_date",'-id')[:limit]

@register.assignment_tag(name='get_featured_items', takes_context=True)
def get_featured_items(context,  limit=3):
    request = context['request']
    current_date = timezone.now()

    max_ = DirectoryItem.objects.values_list('id', flat=True)    
    print max_

    if len(max_) > 0:
        if len(max_) > 1:
            if len(max_)<limit:
                limit = len(max_)
            ids = sample(max_, limit)
        else:
            ids = max_

        return DirectoryItem.objects.filter(
                id__in=ids ,
                publish=True,
                featured_end_date__lte=current_date
            ).order_by("-featured_end_date",'-id')[:limit]
    else:
        return []
 
@register.assignment_tag(name='get_recent_items', takes_context=True)
def get_recent_items(context,  limit=3):
    request = context['request']
    current_date = timezone.now()

    max_ = DirectoryItem.objects.values_list('id', flat=True)    
    if len(max_) > 0:
        if len(max_) > 1:
            if limit >= len(max_):
                limit = len(max_)
            ids = sample(max_, limit)
        else:
            ids = max_

        return DirectoryItem.objects.filter(
                id__in=ids ,
                publish=True,
                
            ).order_by("-featured_end_date",'-id')[:limit]
    else:
        return []

@register.assignment_tag(name='get_random_featured_item', takes_context=True)
def get_random_featured_item(context):
    
    current_date = timezone.now()
    max_ = DirectoryItem.objects.values_list('id', flat=True) 
    if len(max_) > 0:   
        pk = choice(max_)
        return next(
            iter(DirectoryItem.objects.filter(
                    pk=pk, 
                    publish=True,
                    #featured_end_date__lte=current_date
                )
            ), 
            None
        )
    return None

@register.assignment_tag(name='get_recent_items_category_id', takes_context=True)
def get_recent_items_category_id(context, category_id,  limit=3):
    request = context['request']
    return DirectoryItem.objects.filter(
            categories__id=category_id
        ).order_by("-publish_date",'-id')[:limit]

@register.assignment_tag(name='get_viewed_item_ids', takes_context=True)
def get_viewed_item_ids(context,  limit=6 ):
    request = context['request']
    viewed_item_ids = request.session.get('viewed_adverts', {})
    print viewed_item_ids
    if bool(viewed_item_ids) == True:
        return DirectoryItem.objects.filter(
                id__in=viewed_item_ids.get('items',[])
            ).order_by("-publish_date",'-id')[:limit]
    return []


@register.assignment_tag(name='list_categories', takes_context=True)
def list_categories(context,  limit=3 ):
    request = context['request']
    return DirectoryCategory.objects.filter(publish=True)

@register.assignment_tag(name='media_image_list', takes_context=True)
def media_image_list(context,  item):
    request = context['request']
    
    return MediaImage.objects.filter(album__id=item).order_by('id')

@register.assignment_tag(name='media_video_list', takes_context=True)
def media_video_list(context,  item):
    request = context['request']
    
    return MediaVideos.objects.filter(album__id=item).order_by('id')


@register.assignment_tag(name='media_image_ramdom_list', takes_context=True)
def media_image_ramdom_list(context,  item):
    request = context['request']
    ids = [int(x.id) for x in item.album.select_related() ] 
    i = MediaImage.objects.filter(album__id__in=ids).order_by('id')
    print i
    if len(i) > 0:
        t = next(iter(i), None)
        return t
    else: 
        return None

@register.assignment_tag(name='media_image_not_featured_list', takes_context=True)
def media_image_not_featured_list(context,  item):
    request = context['request']
    ids = [int(x.id) for x in item.album.select_related() ] 
    images = MediaImage.objects.filter(
            album__id__in=ids,
            featured=False
        ).order_by('id')
    return images



DATE_FIELD_MAPPING = {
    MediaImage: 'created',
    MediaVideos: 'created',
}

def my_key_func(obj):
    return getattr(obj, DATE_FIELD_MAPPING[type(obj)])

@register.assignment_tag(name='media_image_video_not_featured_list', takes_context=True)
def media_image_video_not_featured_list(context,  item):
    request = context['request']
    ids = [int(x.id) for x in item.album.select_related() ] 
    videos = MediaVideos.objects.filter(
            album__id__in=ids,
            featured=False
        ).order_by('-id')
    images = MediaImage.objects.filter(
            album__id__in=ids,
            featured=False
        ).order_by('-id')
    return sorted(
            itertools.chain(
                videos,
                images,
            ),
            key=my_key_func,
            reverse=True
        )


@register.assignment_tag(name='media_image_featured_list', takes_context=True)
def media_image_featured_list(context,  item):
    request = context['request']
    ids = [int(x.id) for x in item.album.select_related() ] 
    images = MediaImage.objects.filter(
            album__id__in=ids,
            featured=True
        ).order_by('id')
    return images

@register.assignment_tag(name='media_image_video_featured_list', takes_context=True)
def media_image_video_featured_list(context,  item):
    request = context['request']
    ids = [int(x.id) for x in item.album.select_related() ] 
    videos = MediaVideos.objects.filter(
            album__id__in=ids,
            featured=True
        ).order_by('-id')
    images = MediaImage.objects.filter(
            album__id__in=ids,
            featured=True
        ).order_by('-id')
    print images
    print videos
    return sorted(
            itertools.chain(
                videos,
                images,
            ),
            key=my_key_func,
            reverse=True
        )



@register.filter(name='count_no_featured_images')
def count_no_featured_images(item): 
    album_ids = [int(x.id) for x in item ]
    s = MediaImage.objects.filter(
        album__id__in=album_ids        
    ).exclude(featured=True)
    return len(s)

mysystem = [
    (1000 ** 5, ' Megamanys'),
    (1000 ** 4, ' Lotses'),
    (1000 ** 3, ' Tons'), 
    (1000 ** 2, ' Heaps'), 
    (1000 ** 1, ' Bunches'),
    (1000 ** 0, ' Thingies'),
]


@register.filter(name='get_visit_counter')
def get_visit_counter(item): 
   
    return size(item, system=mysystem)

@register.assignment_tag(name='get_placeholder')
def get_placeholder(item): 
   
    return "placeholder:%s" % (_(item))

@register.filter(name='get_price_format')
def get_price_format(item): 
    return '{:,.2f}'.format(item)