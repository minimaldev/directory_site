from django import forms
import os
import sys
import hashlib
from collections import OrderedDict
from user_site import authenticate
from user_site import get_user_model
from user_site.models import UserSite
from django.utils.text import capfirst
from django.contrib.auth.hashers import (
    check_password, make_password)
from django.utils.datastructures import SortedDict
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import get_current_site
from django.utils.encoding import force_bytes
from django.template import loader
from django.utils.http import urlsafe_base64_encode
from django.conf import settings
from django.utils.text import slugify

class AuthenticationForm(forms.Form):

    email = forms.EmailField(label=_("Email"), max_length=254)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': _("Please enter a correct %(email)s and password. "
                           "Note that both fields may be case-sensitive."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):

        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        UserModel = UserSite
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['email'].label is None:
            self.fields['email'].label = capfirst(self.username_field.verbose_name)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            self.user_cache = authenticate(
                email=email,
                password=password
            )
            print 
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'email': self.username_field.verbose_name},
                )
            #elif not self.user_cache.is_active:
            #    raise forms.ValidationError(
            #        self.error_messages['inactive'],
            #        code='inactive',
            #    )
        return self.cleaned_data

    def check_for_test_cookie(self):
        warnings.warn("check_for_test_cookie is deprecated; ensure your login "
                "view is CSRF-protected.", DeprecationWarning)

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

class SetPasswordForm(forms.Form):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'old_password_mismatch': _("The old password fields didn't match."),
    }

    new_password1 = forms.CharField(
            label=_("New password"),
            widget=forms.PasswordInput
        )
    new_password2 = forms.CharField(
            label=_("Repeat new password"),
            widget=forms.PasswordInput
        )
    def __init__(self, user_site, *args, **kwargs):
        self.user_site = user_site
        super(SetPasswordForm, self).__init__(*args, **kwargs)


    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )

        return password2

    def save(self, commit=True):        
        self.user_site.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user_site.save()
        return self.user_site

class SetPasswordForm2(SetPasswordForm):
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
        'security_answer_incorrect': _("Your Security Answer was entered incorrectly."),
        'security_question_incorrect': _("Your Security Question was entered incorrectly."),
    })

    security_answer = forms.CharField(
            label=_("Your Answer *"),
        )

    security_question = forms.CharField(
            label=_("Your Security Question *"),
        )

    def clean_security_answer(self):

        security_answer = self.cleaned_data["security_answer"]

        if not self.user_site.security_answer == security_answer:
            raise forms.ValidationError(
                self.error_messages['security_answer_incorrect'],
                code='security_answer_incorrect',
            )
        return security_answer
    
    def clean_security_question(self):

        security_question = self.cleaned_data["security_question"]

        if not self.user_site.security_question == security_question:
            raise forms.ValidationError(
                self.error_messages['security_question_incorrect'],
                code='security_question_incorrect',
            )
        return security_question

class PasswordChangeForm(SetPasswordForm):

    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
        'security_answer_incorrect': _("Your Security Answer was entered incorrectly."),
        'security_question_incorrect': _("Your Security Question was entered incorrectly."),
    })

    old_password = forms.CharField(
            label=_("Old password"),
            widget=forms.PasswordInput
        )

    def clean_old_password(self):

        old_password = self.cleaned_data["old_password"]
        if not self.user_site.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

PasswordChangeForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)

class UserSiteAdminForm(forms.ModelForm):
    email = forms.EmailField(label=_("Email"), max_length=254)
    
    password = ReadOnlyPasswordHashField(
        label=_("Password"),
        help_text=_("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = UserSite
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UserSiteAdminForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial["password"]

class UserSiteCreationForm(forms.ModelForm):
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    email = forms.EmailField(label=_("Email"), max_length=254)

    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput)

    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = UserSite
        fields = '__all__'

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserSiteCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
"""
agregar el company country estate and zip
"""
class UserSiteProfileForm(forms.ModelForm):

    class Meta:
        model = UserSite
        exclude = (
                'nick_slug',
                'is_active',
                'last_login',
                'password',
            )
        widgets = {}
    def __init__(self, *args, **kwargs):        
        super(UserSiteProfileForm, self).__init__(*args, **kwargs)
    def save(self, commit=True):
        if commit == True:
            self.instance.nick_slug = slugify(self.instance.nick)
            self.instance.save()
            
            if self.files.get('logo', None) is not None:
                self.instance.update_logo()
        return self.instance       

class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=254)

    def save(self, domain_override=None,
             subject_template_name='registration/email/password_reset_subject.txt',
             email_template_name='registration/email/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        from django.core.mail import send_mail
        UserModel = get_user_model()
        email = self.cleaned_data["email"]
        active_users = UserModel._default_manager.filter(
            email__iexact=email, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            c = {
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            email = loader.render_to_string(email_template_name, c)
            send_mail(subject, email, from_email, [user.email])

class UserSiteProfileImagesForm(forms.ModelForm):

    class Meta:
        model = UserSite
        exclude = (
                'nick_slug',
                'is_active',
                'last_login',
                'password',
                'first_name',
                'last_name',
                'nick',
                'nick_slug',
                'email',
                'is_active',
                'marketing',
            )
        widgets = {}

    def save(self, commit=True):  
        if commit == True:
            self.instance.save()         
            if self.files.get('logo', None) is not None:
                self.instance.update_logo()
        return self.instance       
