from django import template
from videos.models import VideoCategory
from videos.models import VideoItem
import urlparse 
register = template.Library()

#@register.inclusion_tag('blog/templatetags/breacrumb.html', takes_context = True)
#def render_breadcrumb_tag(context):	
#	slug=context['Slug']
#	breadcrumb = enumerate(BlogCategory.get_ancestors(ascending=False, include_self=True))
#	if context.has_key('Breadcrumb_last_item'):
#		last_item=context['Breadcrumb_last_item']
#		return {'breadcrumb': breadcrumb,'last_item':last_item}
#	return {'breadcrumb': breadcrumb}


@register.inclusion_tag('blog/videos/templatetags/recent_videos.html', takes_context = True)
def recent_videos(context):	
	
	return {'videos': VideoItem.objects.filter(publish=True)[:4]}

@register.inclusion_tag('blog/videos/templatetags/recent_videos.html', takes_context = True)
def recent_videos_details(context):	
	
	return {'videos': VideoItem.objects.filter(publish=True).exclude(slug=context['video'].slug)[:4]}

@register.filter(name='get_video_Embed')
def get_video_Embed(link):
	hostname=urlparse.urlparse(link).hostname
	if 'youtube' in hostname :
		return link.replace('watch?v=','embed/')
	return ''

